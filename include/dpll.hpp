#ifndef _HPP_DPLL
#define _HPP_DPLL

#include <random>
#include <climits>
#include <iostream>
#include <vector>
#include <stack>
#include <list>
#include <algorithm>
#include "../include/clause.hpp"
#include "../include/graph.hpp"

#include "../include/theory.hpp"

class Dpll
{
public:
        Dpll(unsigned int v,
             std::vector<Clause> formul,
             bool WL = false,
             bool cl = false,
             bool graph = false,
             bool TH = false,
             Theory* theory = NULL,
             bool p_RAND = false,
             bool p_MOMS = false,
             bool p_DLIS = false,
             bool VSIDS = false,
             int VSIDS_COUNT = 5,
	     int p_VERBOSITY = 0);

        std::vector<Litteral> solve();

protected:
	//! Index de chaque WL pour aller plus vite ?
	std::vector<std::list<int> > watchedLitterals; 

	//! Pour les statistiques. Nombre de conflits
	unsigned int nb_conflicts;
	//! Pour les statistiques. Nombre de décisions
	unsigned int nb_decisions;
	//! Pour les statistiques. Nombre de clauses apprises
	unsigned int nb_cllearned;
        //! Nombre de variables
        unsigned int v;
        //! Numéro de la clause faisant conflit (CL)
        unsigned int nc_clause;
	//! Nombre clauses à l'état initial
	unsigned int c;

        //! Niveau actuel de choix. Initalisé à deux pour que rien ne soit mort avant 1.
        unsigned int level; 
        //! Niveau de backtrack auquel retourner
        unsigned int to_bt;
    
	//! Dans le cas des watched litterals : file des variables qu'il reste à propager
	std::list<Litteral> to_prop;

	//! Assignations au niveau courant, pour voir ce qu'il se passe (Rendu2/Bonus1)
	std::stack<Litteral> propagations;

        //! Formules du problème
        std::vector<Clause> formules;
	//! Justifications pour le clause learning : on justifie l'assignation du littéral +-?i par le numéro de clause j, ie justifications[i.var] = j
	std::vector<int> justifications;

        //! À retourner si pas de solution
        std::vector<Litteral> no_sol = std::vector<Litteral>(0);
        //! La solution en cours ou à retourner
        std::vector<Litteral> solution;
        //! La pile des **choix** faits
        std::stack<Litteral> choix; 

        //! Activation ou non des *Watched Litterals*
        bool WL;
        //! Activation ou non du *Clause Learning
        bool cl;
        //! Activation ou non la génération du graphe de résolution, ainsi que le mode interactif (note : si vrai, alors *cl* est automatiquement passé à vrai aussi.
        bool graph;
        //! fichier éventuel de sortie du graphe
        Graph* graph_drawer = new Graph();

        //! Active ou non la gestion d'une théorie
        bool TH;
        //! Pointeur vers la théorie à utiliser (NULL si TH=0).
        Theory* theory;

        //! Active le choix par RAND
        bool RAND;
        //! Active le choix par MOMS
        bool MOMS;
        //! Active le choix par DLIS
        bool DLIS;
        //! Active le choix par VSIDS
        bool VSIDS;
        //! pas de divison de VSIDS (divise par 2 le score tous les VSIDS_COUNT)
        int VSIDS_COUNT;

        //! scores des littéraux, utilisés pour VSIDS. i=1.. v numéro du littéral (idem que dans solution).
        std::vector<unsigned int> vsids_score;
        unsigned int vsids_count = 0;


	//! Active le mode un peu plus bavard
	int VERBOSITY;

        bool bcp();
        int cons_prop();
        bool assume(Litteral is_true);
        bool choose();
        bool backtrack();

        unsigned int learn();
};

#endif

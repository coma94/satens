#ifndef PARSE_BOOL_HPP
#define PARSE_BOOL_HPP

#include "../include/expr.hpp"

int next(int i);

BOOST_PHOENIX_ADAPT_FUNCTION(int, next_, next, 1)



template <typename It, typename Skipper = qi::space_type>
    struct parser : qi::grammar<It, expr(), Skipper>
{
    parser() : parser::base_type(expr_)
    {
        using namespace qi;
        expr_  = impl_.alias();

        impl_= (or_ >> "=>" >> impl_)   [ _val = phx::construct<binop<op_or>>(phx::construct<unop <op_not>>(qi::_1),qi::_2) ] | or_    [ _val = qi::_1 ]; // A=> B     <=> non A ou B
        or_  = (and_ >> "\\/"  >> or_ ) [ _val = phx::construct<binop<op_or >>(qi::_1, qi::_2) ] | and_   [ _val = qi::_1 ];
        and_ = (not_ >> "/\\" >> and_)  [ _val = phx::construct<binop<op_and>>(qi::_1, qi::_2) ] | not_   [ _val = qi::_1 ];
        not_ = ("~" > simple       )    [  _val = phx::construct<unop <op_not>>(qi::_1)     ]     | simple [ _val = qi::_1 ];

        simple = (('(' > expr_ > ')') | var_);
	var_ = int_ [_val = next_(_1) ];
    }
    
private:
    qi::rule<It, var() , Skipper> var_;
    qi::rule<It, expr(), Skipper> not_, and_,  or_, impl_, simple, expr_;
};


bool do_tseitin(std::string input, probleme& pb);

#endif

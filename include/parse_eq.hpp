#ifndef PARSE_EQ_HPP
#define PARSE_EQ_HPP

#include "../include/theories/equality.hpp"
#include "../include/expr.hpp"

int adde(int i1, int i2);

BOOST_PHOENIX_ADAPT_FUNCTION(int, adde_, adde, 2)

template <typename It, typename Skipper = qi::space_type>
    struct eq_parser : qi::grammar<It, expr(), Skipper>
{
    eq_parser() : eq_parser::base_type(expr_)
    {
        using namespace qi;

	expr_  = impl_.alias();
        impl_= or_ [ _val = qi::_1 ] >> -("=>" >> impl_ [ _val = phx::construct<binop<op_or>>(phx::construct<unop <op_not>>(_val),qi::_1) ]); // A=> B     <=> non A ou B
        or_  = and_ [_val = qi::_1] >> -("\\/"  >> or_  [ _val = phx::construct<binop<op_or >>(_val, qi::_1) ]);
        and_ = not_ [_val = qi::_1] >> -("/\\" >> and_  [ _val = phx::construct<binop<op_and>>(_val, qi::_1) ]);
        not_ = ("~" > simple       )    [ _val = phx::construct<unop <op_not>>(qi::_1)     ]     | simple [ _val = qi::_1 ];

        simple = (('(' > expr_ > ')') | var_);
	var_ = (qi::lexeme[char_('x') >> int_ [qi::_a = _1]] >> "=" >> qi::lexeme[char_('x') >> int_ [qi::_b = _1]]) [ _val = adde_(qi::_a, qi::_b)] | (qi::lexeme[char_('x') >> int_ [qi::_a = _1]] >> "!=" >> qi::lexeme[char_('x') >> int_ [qi::_b = _1]]) [ _val = phx::construct<unop<op_not>>(adde_(qi::_a, qi::_b))];
    }
    
private:
    qi::rule<It, expr() , qi::locals<int, int>, Skipper> var_;
    qi::rule<It, expr(), Skipper> not_, and_,  or_, impl_, simple, expr_;
};

bool do_eq(std::string input, Equality& eq, probleme& p);

#endif

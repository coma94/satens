#ifndef _HPP_LITT
#define _HPP_LITT

#include "thing.hpp"
#include <iostream>

enum class states {UNKNOWN, TRUE, FALSE};


class Litteral : public Thing
{
public:
    Litteral();

    /**
     * \fn Litteral(unsigned int vari)
     * \brief Construit le littéral vivant avec un état inconnu (non signé).
     *
     * \param vari variable non signée du littéral.
     */
    Litteral(unsigned int vari);
        
    /**
     * \fn Litteral(int var_signed)
     * \brief Construit le littéral vivant (signé).
     * \param var_signed variable du littéral, son signe est celui du littéral crée.
     */
    Litteral(int var_signed);
        
    unsigned int var;
    states state;
    /**
     * soit il est à vrai et cela veut dire que c'est le cote "pile"
     * (C'est notre premier pari, il n'a pas encore été invalidé) du
     * choix, soit il est à faux et c'est le cote "face" du choix
     */
    bool isfc;
};

/**
 * \fn bool operator==(const Litteral& l, const Litteral& r)
 * Egaux ssi état et variable égales (peu importe la vie et la mort).
 */
bool operator==(const Litteral& l, const Litteral& r);
bool operator!=(const Litteral& l, const Litteral& r);

/*
 * Comparaison de la variable des littéraux
 */
bool operator>=(const Litteral& l, const Litteral &r);
bool operator>(const Litteral& l, const Litteral &r);
bool operator<=(const Litteral& l, const Litteral &r);
bool operator<(const Litteral& l, const Litteral &r);

/**
 * \fn Litteral operator-(const Litteral& l)
 * Change l'état du littéral en l'opposé
 */
Litteral operator-(const Litteral& l);

std::ostream &operator<<(std::ostream &os, Litteral const &L);
std::ostream &verbosedisplay(std::ostream &os, Litteral const &L);
#endif

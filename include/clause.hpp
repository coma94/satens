#ifndef _HPP_CLAUSE
#define _HPP_CLAUSE

#include "thing.hpp"
#include "litteral.hpp"
#include <vector>
#include <list>
#include <iostream>
#include <stack>

class Clause : public Thing{
public:

        Clause();
        
        /**
         * \param WL Active ou non la méthode "Watched Litterals" (défaut à *false*).
         */
        Clause(bool WL);

        /**
         * \fn void add_litt(Litteral L)
         * \brief Ajoute le littéral *L* à la clause.
         */
        void add_litt(Litteral L);

        /**
         * \fn  bool is_in(unsigned int var) const
         * \brief Teste si la variable *var* est dans la clause.
        */
        bool is_in(unsigned int var) const;
    
/**
 * \brief retourne le nombre de littéraux modifiés par le niveau level
 */
    unsigned int nb_new(unsigned int level, std::vector<Litteral>& WL_solution) const;
   
    /**
     * \brief Teste si le litteral L apparait dans la clause
     * \return Vrai ssi L est dans la clause
     */
    bool is_in(Litteral L) const;

        /**
         * \brief Teste si la clause est vide (ie vide en taille OU tout ses littéraux sont morts)
         */
        bool is_empty() const;

        bool assume(Litteral is_true, unsigned int level);
        
    bool assume(Litteral is_true, unsigned int level, std::vector<Litteral>& WL_solution, int numero_clause, std::vector<int>& justifications, std::stack<Litteral>& propagations);
        /**
         * \fn bool assume(Litteral is_true, unsigned int level)
         * \brief Modifie la clause sachant que *is_true* est vrai.
         *
         * \param is_true représente le littéral considéré comme vrai (il peut être négatif).
         * \param level représente le niveau dans l'algo (étape de choix à
         *  laquelle la décision a été faite).
         * \param WL_solution *(si WL)* assignations déjà connues (solution actuelle) pour éventuellement déplacer les léttéraux surveillés.
	 * \param numero_clause : le numéro de la clause sur laquelle on assume (on en a besoin pour le CL)
         * \param les justifications d'assignations (on en a besoin pour le CL)
         * \return faux si une contradiction est détectée (clause vide ou impossible de passer is_true à vrai compte tenu de la solution). Retourne vrai sinon.
        */
    bool assume(Litteral is_true, unsigned int level, unsigned int p_v, std::vector<Litteral>& WL_solution, int numero_clause, std::vector<int>& justifications, std::stack<Litteral>& propagations, std::list<Litteral>& to_prop);

        /**
         * \brief renvoie le nombre de littéraux vivants
         */
        unsigned int alive_size() const;

        /**
         * \brief renvoie la taille du vecteur m_litt
         */
        unsigned int size() const;
        
        /**
         * \fn void backtrack_to(unsigned int level)
         * \brief Remet la clause à l'étape *level*.
         * backtrack(0) remet la clause à son état initial (tout vivant).
        */
        void backtrack_to(unsigned int level);
        
    /**
     * \brief Renvoie le litteral vers lequel pointe WL_w1
     */
    Litteral get_WL1() const;

    /**
     * \brief Renvoie le litteral vers lequel pointe WL_w2
     */
    Litteral get_WL2() const;

    /**
     * \brief ajoute des watched litterals à la clause (ou s'il n'en trouve pas, ajoute des trucs à propager)
     */
    void add_WL(const std::vector<Litteral>& solution, std::list<Litteral>& to_prop, unsigned int to_backtrack);
       

        // iterators
        std::vector<Litteral>::const_iterator begin() const;
        std::vector<Litteral>::const_iterator end() const;
        
        friend Clause operator+(const Clause& c1, const Clause& c2);
    friend Clause resolve(unsigned int to_avoid, const Clause& c1, const Clause& c2, bool WL,  std::vector<Litteral>& solution);
    Litteral get(unsigned int pos);
        friend std::ostream &operator<<(std::ostream &os, Clause const &C);
	friend std::ostream &verbosedisplay(std::ostream& os, Clause const& C);
        bool m_WL = false;

protected:
        std::vector<Litteral> m_litt;
        int WL_w1 = -1;
        int WL_w2 = -1;
        bool m_done = false;
};

/*
 * Fusion de deux Clauses tirées en une clause triée. (Suppression de doublons)).
 !! Attention fonction non compatible avec Watched Litterals !!
 */
Clause operator+(const Clause& c1, const Clause& c2);


/**
 * \brief calcule le résolvant par rapport à to_avoid de c1 et c2. 
 * pas compatible WL comme operator+ !
 */
Clause resolve(unsigned int to_avoid, const Clause& c1, const Clause& c2);


//std::ostream &operator<<(std::ostream &os, Clause const &C);


#endif

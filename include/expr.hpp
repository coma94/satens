#ifndef EXP_INEG_HPP
#define EXP_INEG_HPP

#define BOOST_SPIRIT_USE_PHOENIX_V3
#include <algorithm>
#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>
#include <functional>
#include <boost/fusion/include/adapt_struct.hpp>
#include <boost/fusion/include/io.hpp>
#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/phoenix.hpp>
#include <boost/spirit/include/phoenix_core.hpp>
#include <boost/spirit/include/phoenix_operator.hpp>
#include <boost/spirit/include/qi.hpp>
#include <boost/variant/recursive_wrapper.hpp>
#include <boost/program_options.hpp>


namespace po     = boost::program_options;
namespace qi     = boost::spirit::qi;
namespace phx    = boost::phoenix;
namespace spirit = boost::spirit;
namespace ascii  = boost::spirit::ascii;


struct op_or  {};
struct op_and {};
struct op_not {};

typedef int var;
template <typename tag> struct binop;
template <typename tag> struct unop;

typedef boost::variant<var,
		       boost::recursive_wrapper<unop <op_not> >,
		       boost::recursive_wrapper<binop<op_and> >,
		       boost::recursive_wrapper<binop<op_or> >
		       > expr;


struct probleme
{
  int v;
  int c;
  std::vector<std::vector<int> > proposition;
};


BOOST_FUSION_ADAPT_STRUCT(probleme,
			  (int, v)
			  (int, c)
			  (std::vector<std::vector<int> >, proposition));

template <typename tag> struct binop
{
    explicit binop(const expr& l, const expr& r) : oper1(l), oper2(r) { }
    expr oper1, oper2;
};

template <typename tag> struct unop
{
    explicit unop(const expr& o) : oper1(o) { }
    expr oper1;
};

std::ostream& operator<<(std::ostream& os, const expr& e);

struct tseitin : boost::static_visitor<void> {
    int& my_max;
    std::vector<std::vector<int> >& formules;

    tseitin(int& mym, std::vector<std::vector<int> >& f) : my_max(mym), formules(f) { }//my_max = mym; }
    void operator()(const var &v, int p) const {
	std::vector<int> tmp;
        tmp.push_back(-p);
        tmp.push_back((int)v);
        formules.push_back(tmp);
        tmp.clear();
        tmp.push_back(p);
        tmp.push_back(-(int)v);
	formules.push_back(tmp);
    }
    void operator()(const binop<op_and>  &b, int p) const { proceed(0, b.oper1, b.oper2, p); }
    void operator()(const binop<op_or>   &b, int p) const { proceed(1, b.oper1, b.oper2, p); }
    void operator()(const unop<op_not>   &u, int p) const {
	int p1 = my_max + 1;
	my_max++;
        std::vector<int> tmp;
        tmp.push_back(-p);
        tmp.push_back(-p1);
        formules.push_back(tmp);
        tmp.clear();
        tmp.push_back(p);
        tmp.push_back(p1);
        formules.push_back(tmp);
        recurse(u.oper1, p1);
    }

    void proceed(int nop, const expr &l, const expr &r, int p) const {
	int p1 = my_max + 1;
	int p2 = my_max + 2;
	my_max += 2;
	std::vector<int> tmp;
        if (nop == 0) // and operator
        {
            tmp.push_back(-p);
            tmp.push_back(p1);
            formules.push_back(tmp);
            tmp.clear();
            tmp.push_back(-p);
            tmp.push_back(p2);
            formules.push_back(tmp);
            tmp.clear();
            tmp.push_back(p);
            tmp.push_back(-p1);
            tmp.push_back(-p2);
            formules.push_back(tmp);
        } else if (nop == 1) // or operator
        {
            tmp.push_back(-p);
            tmp.push_back(p1);
            tmp.push_back(p2);
            formules.push_back(tmp);
            tmp.clear();
            tmp.push_back(p);
            tmp.push_back(-p1);
            formules.push_back(tmp);
            tmp.clear();
            tmp.push_back(p);
            tmp.push_back(-p2);
            formules.push_back(tmp);
        }
        recurse(l, p1);
        recurse(r, p2);
    }

  private:
    template <typename T, typename U> void recurse(T const &v, U p) const {
        boost::apply_visitor(std::bind(*this, std::placeholders::_1, p), v);
    }
};

struct printer : boost::static_visitor<void>
{
    printer(std::ostream& os) : _os(os) {}
    std::ostream& _os;

    void operator()(const var& v) const { _os << v;}

    void operator()(const binop<op_and>& b) const { print(" & ", b.oper1, b.oper2); }
    void operator()(const binop<op_or >& b) const { print(" | ", b.oper1, b.oper2); }

    void print(const std::string& op, const expr& l, const expr& r) const
    {
        _os << "(";
            boost::apply_visitor(*this, l);
            _os << op;
            boost::apply_visitor(*this, r);
        _os << ")";
    }

    void operator()(const unop<op_not>& u) const
    {
        _os << "(";
            _os << "!";
            boost::apply_visitor(*this, u.oper1);
        _os << ")";
    }
};

#endif

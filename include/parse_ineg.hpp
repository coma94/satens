#ifndef PARSE_INEG_HPP
#define PARSE_INEG_HPP

#include "../include/theories/difference.hpp"
#include "../include/expr.hpp"


int addi(int i1, int i2, int c);


BOOST_PHOENIX_ADAPT_FUNCTION(int, addi_, addi, 3)

template <typename It, typename Skipper = qi::space_type>
    struct diff_parser : qi::grammar<It, expr(), Skipper>
{
    diff_parser() : diff_parser::base_type(expr_)
    {
        using namespace qi;

	expr_  = impl_.alias();
        impl_= or_ [ _val = qi::_1 ] >> -("=>" >> impl_ [ _val = phx::construct<binop<op_or>>(phx::construct<unop <op_not>>(_val),qi::_1) ]); // A=> B     <=> non A ou B
        or_  = and_ [_val = qi::_1] >> -("\\/"  >> or_  [ _val = phx::construct<binop<op_or >>(_val, qi::_1) ]);
        and_ = not_ [_val = qi::_1] >> -("/\\" >> and_  [ _val = phx::construct<binop<op_and>>(_val, qi::_1) ]);
        not_ = ("~" > simple       )    [ _val = phx::construct<unop <op_not>>(qi::_1)     ]     | simple [ _val = qi::_1 ];

        simple = (('(' > expr_ > ')') | var_);
	var_ = (qi::lexeme[char_('x') >> int_ [qi::_a = _1]] >> "-" >> qi::lexeme[char_('x') >> int_ [qi::_b = _1]] >> "<=" >> int_[qi::_c = _1]) [ _val = addi_(qi::_a, qi::_b, qi::_c) ]
	    | (qi::lexeme[char_('x') >> int_ [qi::_a = _1]] >> "-" >> qi::lexeme[char_('x') >> int_ [qi::_b = _1]] >> "=" >> int_[qi::_c = _1]) [ _val = phx::construct<binop<op_and>>(addi_(qi::_a, qi::_b, qi::_c), addi_(qi::_b, qi::_a, -qi::_c))] // x-y = c 
	    | (qi::lexeme[char_('x') >> int_ [qi::_a = _1]] >> "-" >> qi::lexeme[char_('x') >> int_ [qi::_b = _1]] >> "!=" >> int_[qi::_c = _1]) [ _val = phx::construct<unop<op_not>>(phx::construct<binop<op_and>>(addi_(qi::_a, qi::_b, qi::_c), addi_(qi::_b, qi::_a, -qi::_c)))] // x-y != c
	    | (qi::lexeme[char_('x') >> int_ [qi::_a = _1]] >> "-" >> qi::lexeme[char_('x') >> int_ [qi::_b = _1]] >> ">" >> int_[qi::_c = _1]) [ _val = phx::construct<unop<op_not>>(addi_(qi::_a, qi::_b, qi::_c))] // x-y > c
	    | (qi::lexeme[char_('x') >> int_ [qi::_a = _1]] >> "-" >> qi::lexeme[char_('x') >> int_ [qi::_b = _1]] >> "<" >> int_[qi::_c = _1]) [ _val = addi_(qi::_a, qi::_b, (qi::_c) - 1)] // x-y < c
	    | (qi::lexeme[char_('x') >> int_ [qi::_a = _1]] >> "-" >> qi::lexeme[char_('x') >> int_ [qi::_b = _1]] >> ">=" >> int_[qi::_c = _1]) [ _val = phx::construct<unop<op_not>>(addi_(qi::_a, qi::_b, qi::_c - 1))]; // x-y >= c

    }
    
private:
    qi::rule<It, expr() , qi::locals<int, int, int>, Skipper> var_;
    qi::rule<It, expr(), Skipper> not_, and_,  or_, impl_, simple, expr_;
};

bool do_ineq(std::string input, Difference& diff, probleme& pb);

#endif

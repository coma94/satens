#ifndef _HPP_CONGRUENCE
#define _HPP_CONGRUENCE

#include <list>
#include <string>
using namespace std;

#include "../theory.hpp"



class Term{
public:
        Term(string name, list<Term> args);
        
        string name;
        list<Term> subterms;
};

std::ostream &operator<<(std::ostream &os, Term const &T);


class Congruence : public Theory
{
        Congruence();
};

#endif

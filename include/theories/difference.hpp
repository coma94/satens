#ifndef _HPP_DIFFERENCE
#define _HPP_DIFFERENCE

#include <list>
#include <string>
#include <vector>
#include <climits>

#include "../theory.hpp"
#include "../litteral.hpp"
#include "../thing.hpp"

struct Ineq{ // représente l'inégalité x1 - x2 <= c
    Litteral litt;
    int x1;
    int x2;
    int c;
};

bool operator==(const Ineq& i1, const Ineq& i2);
bool operator!=(const Ineq& i1, const Ineq& i2);

bool compIneqLitt(const Ineq& i1, const Ineq& i2);


struct Arete : Thing{
    int dest;
    int poids;
};


class Difference : public Theory
{
public:
    Difference();
    ~Difference();


	/**
	 *
	 */
    void add_ineq(Ineq i);
    int add_ineq(int item1, int item2, int c);
    int get_number();
    void backtrack_to(unsigned int level);
    Clause add_assignation(Litteral is_true, unsigned int level);
    Clause check_neg_cycle(int source);
    bool check_validity();
    void print_sol();
protected:
    //! On va faire des piles d'adjacence (plus simple pour backtracker)
    std::list<Ineq> donnees;

    //! Graphe représenté par des listes d'adjacence
    std::vector<std::list<Arete> > graphe;
    std::vector<int> peres;
    std::vector<int> d;
    //! Numéro des littéraux encore non utilisés
    unsigned int number = 0;
};

#endif

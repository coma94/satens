#ifndef _HPP_CONGRUENCE
#define _HPP_CONGRUENCE

#include <list>
#include <string>

#include "union_find.hpp"

#include "../theory.hpp"
#include "../litteral.hpp"
#include "../clause.hpp"

struct Eq{
        Litteral litt;
        std::string item1;
        std::string item2;
};

        

class Equality : public Theory
{
public:
        Equality();
        ~Equality();

        int add_eq(std::string item1, std::string item2);
    int get_number();

        void backtrack_to(unsigned int level);
        Clause add_assignation(Litteral is_true, unsigned int level);

        bool check_validity();

        void print_sol();
        

protected:
        void add_eq(Eq eq);
        Clause check_neqs();
        
        std::list<Eq> assignations;
        
        //! Numéro des littéraux "fresh"
        unsigned int number = 1;

        //! Union find
        UnionFind<std::string> uf;
        
};


#endif

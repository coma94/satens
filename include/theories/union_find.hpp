#ifndef _HPP_UNION_FIND
#define _HPP_UNION_FIND

#include <map>
#include <boost/pending/disjoint_sets.hpp>

/**
 * An union-find class with history. Type T.
 */

template <typename T>
class UnionFind
{
public:
        UnionFind();

        /**
         * Créer l'ensemble constitué d'un élément, au niveau 0.
         */
        void make_set(const T& item);

        /**
         * Fusionne les classes des éléments 1 et 2 au niveau *level*, qui sera créé s'il n'existe pas.
         */
        void union_set(const T& item, const T& item2, unsigned int level);

        /**
         * Donne la classe représentante de l'élément *item*, au niveau *level*, défini au dernier niveau par défaut.
         */
        T find_set(T item, unsigned int level);

        /**
         * Donne la classe représentante de l'élément *item*, au dernier niveau.
         */
        T find_set(T item);
        
        /*
         * Supprime les états strictement supérieurs à *level* et le définit comme nouvel état maximal.
         */
        void backtrack_to(unsigned int level);

        
protected:
        // boost union find
        typedef std::map<T,std::size_t> rank_t; // => order on Element
        typedef std::map<T,T> parent_t;
        
        std::map<unsigned int, rank_t> rank_map;
        std::map<unsigned int, parent_t> parent_map;

         
        std::map<unsigned int, boost::associative_property_map<rank_t> > rank_pmaps;
        std::map<unsigned int,boost::associative_property_map<parent_t> > parent_pmaps;

        typedef boost::disjoint_sets<boost::associative_property_map<rank_t>,boost::associative_property_map<parent_t> > dset_type;
        std::map<unsigned int, dset_type* > dsets;
        
        //! Dernier level en cours de remplissage
        unsigned int last_level=0;
};

#include "../../src/theories/union_find.tpp"

#endif

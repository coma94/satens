#ifndef _HPP_THING
#define _HPP_THING

/**
 * \class Thing
 * \brief Objet pouvant être vivant ou mort.
 *  
 * Typiquement, une clause ou un littéral.
*/
class Thing{
public:
        /**
         * \fn unsigned int is_dead() const
         * \brief Teste si la chose est morte et retourne l'étape de la mort.
         *
         * \return **0** :  vivant
         * \return **other** : la date (ou *niveau*) à laquelle la chose été tuée.
        */
        unsigned int is_dead() const;
        
        /**
         * \fn void kill(unsigned int level)
         * \brief Tue la chose.
         *
         * \param level niveau auquel la chose est tuée.
        */
        void kill(unsigned int level);

        /**
         * \fn void resurrect()
         * \brief Rend la chose vivante.
        */
        void resurrect();
        
protected:
        /**
         * \brief Contient l'étape à laquelle la chose est morte.
         * \value 0 si vivante.
        */
        unsigned int m_dead;
};


#endif

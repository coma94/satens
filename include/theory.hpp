#ifndef _HPP_THEORY
#define _HPP_THEORY

#include <list>

#include "litteral.hpp"
#include "clause.hpp"

class Theory
{
public:
        Theory();
        ~Theory();
        
        /**
         * \brief Backtrack les assignations connues
         * 
         * Remet les assignation enregistrées à leur état au n-iveau *level*.
         */
        virtual void backtrack_to(unsigned int level) = 0;

        /**
         * \brief Ajoute une assignation connue à la théorie.
         * ! Fonction vituelle pure, nécessited d'être implémentée pour chaque théorie.
         * \param is_true littéral considéré vrai à ajouter
         * \param level niveau auquel le littéral est ajouté (utilisé pour le backtrack)
         * \return La clause vide si pas de problème
         * \return Une clause "exprimant" le conflit en cas de contradiction avec la théorie
         */
        virtual Clause add_assignation(Litteral is_true, unsigned int level) = 0;

        /**
         * \brief Teste la validité de la théorie.
         * Teste à l'état actuel au niveau maximal.
         * \return true si tout va bien
         * \return false si contradiction
         */
        virtual bool check_validity() = 0;

        /**
         * \brief Affiche la solution
         * ! Fonction vituelle pure, nécessited d'être implémentée pour chaque théorie.
         * Affiche la solution à l'écran, compte tenu des assignations actuelles.
         */
        virtual void print_sol() = 0;

protected:
        /**
         * Vecteur des assignations connues.
         *
         * Les assignations sont les valeurs *alpha*, qui correspondent à des "atomes"/"blocs" de la théorie.
         */
        std::list<Litteral> assignations;
};

#endif

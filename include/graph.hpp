#ifndef _HPP_GRAPH
#define _HPP_GRAPH

#include <iostream>
#include <fstream> // IO file
#include <string>

using namespace std;


/**
 * \brief Classe permettant de dessiner un graphe simple
 * Utile pour générer le graphe des contradictions.
 */

class Graph
{
public:
        Graph();
        ~Graph();

        void add_vertex(string name, string color = "black", string style = "\"\"", bool top=false);
        void add_vertex(unsigned int name, string color = "black", string style = "\"\"", bool top=false);
        
        void add_edge(string vertex1, string vertex2);
        void add_edge(unsigned int vertex1, string vertex2); 
        void add_edge(unsigned int vertex1, unsigned int vertex2); 
        
        void store(string filename);
    void clear();
protected:
        string text;
};




#endif

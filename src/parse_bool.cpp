#include "../include/parse_bool.hpp"


int m = 0;

int next(int i)
{
//    std::cout << "\tCALLED NEXT\n";
    if(m < i)
    {
	m = i;
    }
    return i;
}


/**
 * \brief renvoie true ssi parsing ok, et dans ce cas la tseitinisation est faite dans formules
 */
bool do_tseitin(std::string input, probleme& pb)//std::vector<Clause>& formules)
{
    std::istringstream is(input);
    std::string line;
    std::vector<expr> res;
    std::vector<std::vector<int> > formules;
    auto b = line.begin();
    parser<decltype(b)> p;
    m = 10;
    expr result;
    bool ok = true;
    while(std::getline(is, line))
    {
	auto b = line.begin();
	auto e = line.end();
	ok = ok && qi::phrase_parse(b,e,p,qi::space,result);
	res.push_back(result);
//	std::cout << "parsed : " << line << std::endl;
//	std::cout << result;
    }
    for(expr r : res)
    {
	m++;
	pb.proposition.push_back({m});
	boost::apply_visitor(std::bind(tseitin(m, pb.proposition), std::placeholders::_1, m), r);
    }
    //  std::cout << "parsing done\n";
    pb.v = m;
    pb.c = pb.proposition.size();
    return ok;
}

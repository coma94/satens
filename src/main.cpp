#define BOOST_SPIRIT_USE_PHOENIX_V3
#include <algorithm>
#include <fstream>
#include <iostream>
#include <vector>
#include <functional>
#include <boost/fusion/include/adapt_struct.hpp>
#include <boost/fusion/include/io.hpp>
#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/phoenix.hpp>
#include <boost/spirit/include/phoenix_core.hpp>
#include <boost/spirit/include/phoenix_operator.hpp>
#include <boost/spirit/include/qi.hpp>
#include <boost/variant/recursive_wrapper.hpp>
#include <boost/program_options.hpp>

#include "../include/dpll.hpp"
#include "../include/parse_ineg.hpp"
#include "../include/parse_bool.hpp"
#include "../include/parse_eq.hpp"

namespace po     = boost::program_options;
namespace qi     = boost::spirit::qi;
namespace phx    = boost::phoenix;
namespace spirit = boost::spirit;
namespace ascii  = boost::spirit::ascii;

std::string get_file_contents(const char* filename)
{
        std::ifstream in(filename, std::ios::in | std::ios::binary);
        std::string contents = "";
        if(in)
        {
                in.seekg(0, std::ios::end);
                contents.resize(in.tellg());
                in.seekg(0, std::ios::beg);
                in.read(&contents[0], contents.size());
                in.close();
        }
        return contents;
}	

int max(int a, int b)
{
        if(a > b)
                return a;
        return b;
}

bool comp(int a, int b)
{
	if(abs(a) > abs(b))
		return true;
	else if(abs(a) == abs(b) && a > b)
		return true;
	return false;
}

int main(int nargs, char** args)
{
	bool WL, CL, CLI, TSEITIN, EQ, DIFF, TH;
	std::string filename;
	WL = false;
	CL = false;
	CLI = false;
	TSEITIN = false;
	EQ = false;
	DIFF = false;
	TH = false;

	// Active le choix par RAND
	bool RAND=false;
	// Active le choix par MOMS
	bool MOMS=false;
	// Active le choix par DLIS
	bool DLIS=false;
	// Active le choix par VSIDS
	bool VSIDS=false;
	int VSIDS_COUNT = 5;
	int VERBOSITY = 0;

	Theory* theory = NULL;
    
    
#ifdef DEBUG
        std::cout << "Debug defined\n";
#endif
        // Gestion des arguments
	po::options_description desc("Allowed options");
	desc.add_options()
		("help", "produce this help message")
		("wl", "activate watched litterals")
		("cl", "activate clause learning")
		("cl-interact", "activate interactive mode in clause learning")
		("input-file", po::value<std::string>(), "input file in DIMACS format for the SAT-solver")
		("tseitin", "use tseitin transformation and then solve problem")
		("eq", "use the SMT-solver with the equality logic")
		("diff", "use the SMT-solver with the difference logic")
		("rand", "random choice of literals")
		("moms", "'Maximum Occurrences in clauses of Minimum Size' choice of literals")
		("dlis", "'Dynamic Largest Individual Sum' choice of literals")
		("vsids", "'Variable State Independent Decaying Sum' choice of litterals")
                ("vsids-count", po::value<int>()->implicit_value(5), "'Variable State Independent Decaying Sum' choice of litterals, with a division by 2 each 'arg' turn (default 5)")
		("verbose", po::value<int>()->implicit_value(1), "Verbose mode. (default verbosity 1)")
//	("output-file", po::value<string>(), "graph output file")
		;
	try
	{
		// Declare the supported options.
    
		po::positional_options_description pod;
		pod.add("input-file", -1);

		po::variables_map vm;
		po::store(po::command_line_parser(nargs, args)
			  .options(desc).positional(pod).run(), vm);
		po::notify(vm);    
    
		if (vm.count("help")) {
			std::cout << "Usage: " << args[0] << " [options]\n";
			std::cout << desc << "\n";
			return 1;
		}
		if(vm.count("tseitin"))
		{
#ifdef DEBUG
			std::cout << "TSEITIN\n";
#endif
			TSEITIN = true;
		}
		if(vm.count("eq"))
		{
#ifdef DEBUG
			std::cout << "EQ\n";
#endif
			EQ = true;
			TH = true;
		}
		if(vm.count("diff"))
		{
#ifdef DEBUG
			std::cout << "DIFF\n";
#endif
			DIFF = true;
			TH = true;
		}
		if(vm.count("wl"))
		{
#ifdef DEBUG
			std::cout << "WL\n";
#endif
			WL = true;
		}
		if(vm.count("cl"))
		{
			if(vm.count("cl-interact"))
			{
				std::cout << "Error : please use cl OR cl-interact\n\n";
				std::cout << "Usage: " << args[0] << " [options]\n";
				std::cout << desc << "\n";
				return 1;
			}
			CL = true;
#ifdef DEBUG
			std::cout << "CL\n";
#endif
		}
		if(vm.count("cl-interact"))
		{
			CLI = true;
#ifdef DEBUG
			std::cout << "CL-INTERACT\n";
#endif
		}
		if(vm.count("rand"))
		{
			RAND = true;
#ifdef DEBUG
			std::cout << "RAND\n";
#endif
		}
		if(vm.count("moms"))
		{
			if(RAND)
				std::cout << "! Conflit de méthode de choix, séléctionnée: RAND" << std::endl;
			else{
				MOMS = true;
#ifdef DEBUG
				std::cout << "MOMS\n";
#endif
			}
		}
		if(vm.count("dlis"))
		{
			if(RAND)
				std::cout << "! Conflit de méthode de choix, séléctionnée: RAND" << std::endl;
			else if(MOMS)
				std::cout << "! Conflit de méthode de choix, séléctionnée: MOMS" << std::endl;
			else{
				DLIS = true;
#ifdef DEBUG
				std::cout << "DLIS\n";
#endif
			}
		}
		if(vm.count("vsids"))
		{
			if(RAND)
				std::cout << "! Conflit de méthode de choix, séléctionnée: RAND" << std::endl;
			else if(MOMS)
				std::cout << "! Conflit de méthode de choix, séléctionnée: MOMS" << std::endl;
			else if(DLIS)
				std::cout << "! Conflit de méthode de choix, séléctionnée: DLIS" << std::endl;
			else if(!CL)
				std::cout << "! Clause learnig (--cl) non activé, paramètre --vsids ignoré" << std::endl;
			else{
				VSIDS = true;
#ifdef DEBUG
				std::cout << "VSIDS\n";
#endif
			}
		}
		if(vm.count("vsids-count"))
		{
			if(RAND)
				std::cout << "! Conflit de méthode de choix, séléctionnée: RAND" << std::endl;
			else if(MOMS)
				std::cout << "! Conflit de méthode de choix, séléctionnée: MOMS" << std::endl;
			else if(DLIS)
				std::cout << "! Conflit de méthode de choix, séléctionnée: DLIS" << std::endl;
			else if(!CL)
				std::cout << "! Clause learnig (--cl) non activé, paramètre --vsids ignoré" << std::endl;
			else{
				VSIDS = true;
#ifdef DEBUG
				std::cout << "VSIDS\n";
#endif
			}

			VSIDS_COUNT = vm["vsids-count"].as<int>();
#ifdef DEBUG
			std::cout << "vsids-count: " << VSIDS_COUNT << std::endl;
#endif
		}
		if(vm.count("verbose"))
		{
			VERBOSITY = vm["verbose"].as<int>();
#ifdef DEBUG
			std::cout << "VERBOSITY: " << VERBOSITY << std::endl;
#endif
		}
		if(vm.count("input-file"))
		{
			filename = vm["input-file"].as<std::string>();
#ifdef DEBUG
			std::cout << "Input file is " << vm["input-file"].as<std::string>() << std::endl;
#endif
		}
		else
		{
			std::cout << "Usage: " << args[0] << " [options]\n";
			std::cout << desc << "\n";
			return 1;
		}
	}
	catch(exception& e)
	{
		std::cerr << "Error: " << e.what() << std::endl;
		std::cout << "Usage: " << args[0] << " [options]\n";
		std::cout << desc << "\n";
		return 1;
	}
	catch(...)
	{
		std::cerr << "Unknown exception\n";
		return 2;
	}

        std::string str = get_file_contents(filename.c_str());
	
        probleme p;
	bool r = false;
        Equality e;
        Difference d;
        
	if(EQ)
	{
		r = do_eq(str, e, p);
		if(VERBOSITY > 0)
		{
			std::cout << "Conversion to CNF gives :\n";
			for(auto c : p.proposition)
			{
				for(auto var : c)
				{
					if(var < 0)
						std::cout << var << " ";
					else
						std::cout << "+" << var << " ";
				}
				std::cout << std::endl;
			}

		}
		p.proposition.push_back({0});

		theory = &e;
	}
	else if(DIFF)
	{
		r = do_ineq(str, d, p);
		if(VERBOSITY > 0)
		{
			std::cout << "Conversion to CNF gives :\n";
			for(auto c : p.proposition)
			{
				for(auto var : c)
				{
					if(var < 0)
						std::cout << var << " ";
					else
						std::cout << "+" << var << " ";
				}
				std::cout << std::endl;
			}

		}
		p.proposition.push_back({0});

		theory = &d;
	}
	else if(TSEITIN)
	{
		r = do_tseitin(str, p);
		if(VERBOSITY > 0)
		{
			std::cout << "Conversion to CNF gives :\n";
			for(auto c : p.proposition)
			{
				for(auto var : c)
				{
					if(var < 0)
						std::cout << var << " ";
					else
						std::cout << "+" << var << " ";
				}
				std::cout << std::endl;
			}

		}
		p.proposition.push_back({0});
	}
	else
	{
		std::string::const_iterator b = str.begin();
		std::string::const_iterator e = str.end();
		r = qi::phrase_parse(b, e,  qi::lit("p cnf") >>  qi::int_ >>  qi::int_ >> *(qi::int_ - '0') % '0', (ascii::space | qi::lexeme[qi::lit("c") >> *(qi::char_-qi::eol) >> qi::eol]), p);
	}
        if(!r)
        {
                std::cout << "s ???\n";
                return 255;
        }

        p.proposition.pop_back();
        if(p.c != p.proposition.size())
        {
                std::cout << "Le fichier comporte " << p.proposition.size() << " clauses, alors que " << p.c << " clauses étaient annoncées.\n";
                p.c = p.proposition.size();
        }
        
        int vmax = 0;
#ifdef DEBUG
        std::cout << "Liste des clauses :\n";
#endif
        
        for(int i = 0; i < p.proposition.size(); i++)
        {
		std::sort(p.proposition[i].begin(), p.proposition[i].end(), comp); 
#ifdef DEBUG 
                for(int j = 0; j < p.proposition[i].size(); j++)
                        std::cout << p.proposition[i][j] << " ";
                std::cout << std::endl;
#endif
                if(p.proposition[i].size() > 0)
                        vmax = max(vmax, abs(p.proposition[i][0]));
        }
        if(p.v != vmax)
        {
                std::cout << "Le fichier comporte " << vmax << " variables, alors que " << p.v << " variables étaient annoncées.\n";
                p.v = vmax;
        }
        std::vector<Clause> formules(p.c);

        Litteral tmp;
	Litteral last;
        tmp.resurrect();
        for(int i = 0; i < p.proposition.size(); i++)
        {
                formules[i].m_WL = WL; // WL true => WL
                
                last.state = states::UNKNOWN;
                last.var = 0;
                int j;
                for(j = 0; j < p.proposition[i].size(); j++)
                {
                        if(p.proposition[i][j] >= 0)
                        {
                                tmp.state = states::TRUE;
                                tmp.var = p.proposition[i][j];
                        }
                        else
                        {
                                tmp.state = states::FALSE;
                                tmp.var = - p.proposition[i][j];
                        }
// on a juste à vérifier par rapport au prédécesseur qu'il vaut pas la m^eme sinon on n'ajoute pas
			if(!(tmp == last || tmp == -last))
			{
				formules[i].add_litt(tmp);
				last = tmp;
			}
			else if(tmp == -last)
			{
				// tautologie : pas besoin de continuer la clause
				formules[i].add_litt(tmp);
				formules[i].kill(1); 
				break;
			}
			    
                }
		if(j == p.proposition[i].size() && !(tmp == -last))
		{
			formules[i].resurrect();
		}
        }
	int i = 0;
	while(i < formules.size())
	{
		if(formules[i].is_dead() == 1)
			formules.erase(formules.begin()+i);
		// on supprime les tautologies
		else
			i++;
	}
        std::vector<Litteral> res(1+p.v);

        Dpll dpll(p.v, formules, WL, CL, CLI, TH, theory, RAND, MOMS, DLIS, VSIDS, VSIDS_COUNT, VERBOSITY); 
        res = dpll.solve();
        
        if(res.size() == 0)
                std::cout << "UNSATISFIABLE\n";
        else
        {
                std::cout << "SATISFIABLE\n";
		if(EQ)
		{
#ifdef DEBUG
			std::cout << "printing solution in EQ format\n";
#endif
			e.print_sol();
		}
		else if(DIFF)
		{
#ifdef DEBUG
			std::cout << "printing solution in DIFF format\n";
#endif
			d.print_sol();
		}
		else
		{
			for(int i = 1; i < 1+p.v; i++)
			{
				if(res[i].state == states::TRUE)
					std::cout << res[i].var << " ";
				else
					std::cout << "-" << res[i].var << " ";
			}
			std::cout << std::endl;
		}
        }
#ifdef DEBUG
	std::cout << "end of program" << std::endl;
#endif
        return 0;
}




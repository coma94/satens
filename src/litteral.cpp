#include "../include/litteral.hpp"

Litteral::Litteral()
{}

Litteral::Litteral(unsigned int vari)
	: var((unsigned int) abs(vari)),
	  state(states::UNKNOWN),
	  isfc(false)
{
        this->resurrect();
}

Litteral::Litteral(int vari)
        : var((unsigned int) abs(vari))
{
        if(vari>=0)
                state = states::TRUE;
        else
                state = states::FALSE;
        
        this->resurrect();
}

bool operator==(const Litteral& l, const Litteral& r)
{
        return (l.var == r.var) && (l.state == r.state);
}

bool operator!=(const Litteral& l, const Litteral& r)
{
        return !(l==r);
}

bool operator>(const Litteral& l, const Litteral& r)
{
	if(l.var > r.var)
		return true;
	else if(l.var == r.var && l.state == states::TRUE && r.state != states::TRUE)
		return true;
	else if(l.var == r.var && l.state == states::FALSE && r.state == states::UNKNOWN)
		return true;
	return false;
}

bool operator>=(const Litteral& l, const Litteral &r)
{
	return (l.var > r.var) || (l.var == r.var);
}

bool operator<(const Litteral& l, const Litteral& r)
{
	return !(l >= r);
}

bool operator<=(const Litteral& l, const Litteral& r)
{
	return !(l > r);
}


Litteral operator-(const Litteral& l)
{
        Litteral s = l;
        if(s.state == states::TRUE)
                s.state = states::FALSE;
        else if(s.state== states::FALSE)
                s.state = states::TRUE;
        
        return s;        
}


std::ostream &operator<<(std::ostream &os, Litteral const &L)
{
        if(L.is_dead())
                os << "(";
                
        switch(L.state)
        {
        case states::TRUE:
        {
                os << "+";
                break;
        }
        case states::FALSE:
        {
                os << "-";
                break;
        }
        case states::UNKNOWN:
        {
                os << "?";
                break;
        }
        }

        os << L.var;

        if(L.is_dead())
                os << ")";

        os << ":" << L.is_dead();

        return os;
}

std::ostream &verbosedisplay(std::ostream &os, Litteral const &L)
{
        switch(L.state)
        {
        case states::TRUE:
        {
                os << "+";
                break;
        }
        case states::FALSE:
        {
                os << "-";
                break;
        }
        case states::UNKNOWN:
        {
                os << "?";
                break;
        }
        }

        os << L.var;
        return os;
}

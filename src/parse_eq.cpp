#include "../include/parse_eq.hpp"

Equality eq;

int adde(int i1, int i2)
{
//    std::cout << "CALLED ADDE with " << i1 << " " << i2 << std::endl;
    std::string s1 = "x" + std::to_string(i1);
    std::string s2 = "x" + std::to_string(i2);
    return eq.add_eq(s1, s2);
}

bool do_eq(std::string input, Equality& equa, probleme& pb)
{
//    std::cout << "do eq\n;";
    std::istringstream is(input);
    std::string line;
    auto b = line.begin();
    eq_parser<decltype(b)> p;
    std::vector<expr> res;
    expr result;
    bool ok = true;
    while(std::getline(is, line))
    {
	auto b = line.begin();
	auto e = line.end();
//	std::cout << "PARSING " << line << std::endl;
	ok = ok && qi::phrase_parse(b,e,p,qi::space, result);
	res.push_back(result);
//	std::cout << result;
    }
    int m = eq.get_number();
    for(expr r : res)
    {
//ajouter les racines unitaires à chaque fois
	m++;
	pb.proposition.push_back({m});
	boost::apply_visitor(std::bind(tseitin(m, pb.proposition), std::placeholders::_1, m), r);
    }
//    std::cout << "end of do eq\n";
    pb.v = m;
    pb.c = pb.proposition.size();
    equa = eq;
    return ok;
}

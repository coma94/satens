#include "../include/graph.hpp"

Graph::Graph()
{
        text = "";
}

Graph::~Graph()
{
}


/**
 * Ajoute un sommet.
 *
 * \param name nom du sommet.
 * \param color couleur de l'arête (voir doc de graphviz pour les couleurs)
 * \param style style de sommet (voir doc de graphviz pour voir ce qui existe)
 * \param top insère le sommet en tête de fichier (utile pour modifier la couleur)
 */
void Graph::add_vertex(string name, string color, string style, bool top)
{
        string txt(" ");
        txt += "node [style=" + style + ",color=" + color  + "]\n";
        txt += "\"";
        txt += name;
        txt += "\";\n";

        if(!top)
                text = text + txt;
        else
                text = txt + text;
}

/**
 * Ajoute un sommet.
 *
 * \param name nom du sommet.
 * \param color couleur de l'arête (voir doc de graphviz pour les couleurs)
 * \param style style de sommet (voir doc de graphviz pour voir ce qui existe)
 * \param top insère le sommet en tête de fichier (utile pour modifier la couleur)
 */
void Graph::add_vertex(unsigned int name, string color, string style, bool top)
{
        add_vertex(to_string(name), color, style, top);
}


/*
 * Ajoute une arête *vertex*->*vertex2*.
 *
 */
void Graph::add_edge(string vertex1, string vertex2)
{
        text += "\"";
        text += vertex1;
        text += "\" -> \"";
        text += vertex2;
        text += "\";\n";
}

/*
 * Ajoute une arête *vertex*->*vertex2*.
 *
 */
void Graph::add_edge(unsigned int vertex1, string vertex2)
{
        add_edge(to_string(vertex1), vertex2);
}

/*
 * Ajoute une arête *vertex*->*vertex2*.
 *
 */
void Graph::add_edge(unsigned int vertex1, unsigned int vertex2)
{
        add_edge(vertex1, to_string(vertex2));
}


/**
 * Enregistre le graphe dans le fichier *filename*, et vide le texte du graphe.
 */
void Graph::store(string filename)
{
        ofstream out_graph_file;
        
        out_graph_file.open(filename, ios::out | ios::trunc);
        if(out_graph_file.is_open()){
                out_graph_file << "digraph G {\n";
                out_graph_file << text;
                out_graph_file << "}\n";
                out_graph_file.close();
                
#ifdef DEBUG
                cout << "ok opened\n";
#endif

                text = "";
        }
}

void Graph::clear()
{
    text = "";
}

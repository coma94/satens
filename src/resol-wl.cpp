#include <cstdlib>
#include <iostream>
#include <string>

int main(int nargs, char** args)
{
    if(nargs != 2)
    {
	std::cout << "Usage : " << args[0] << " FILE.cnf" << std::endl;
	return 2;
    }
    else
    {
	std::string path = std::string(args[0]);
	int found = path.find_last_of("/");
	path = path.substr(0, found+1);
	std::string cmd = path + "resol " + args[1] + " --wl";
	system(cmd.c_str());
    }

}

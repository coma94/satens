#include "../include/parse_ineg.hpp"

Difference d;



int addi(int i1, int i2, int c)
{
//    std::cout << "CALLED ADDI with" << std::endl;
    return d.add_ineq(i1, i2, c);
}


bool do_ineq(std::string input, Difference& diff, probleme& pb)
{
//    std::cout << "do ineq\n;";
    std::istringstream is(input);
    std::string line;
    auto b = line.begin();
    diff_parser<decltype(b)> p;
    expr result;
    std::vector<expr> res;
    bool ok = true;
    while(std::getline(is, line))
    {
	auto b = line.begin();
	auto e = line.end();
	ok = ok && qi::phrase_parse(b,e,p,qi::space, result);
//	std::cout << result;
	res.push_back(result);
    }
    int m = d.get_number();
    for(expr r : res)
    {
	m++;
	pb.proposition.push_back({m});
	boost::apply_visitor(std::bind(tseitin(m, pb.proposition), std::placeholders::_1, m), r);	
    }
//    std::cout << "end of do ineq\n";
    pb.v = m;
    pb.c = pb.proposition.size();
    diff = d;
    return ok;
}


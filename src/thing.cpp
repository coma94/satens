#include "../include/thing.hpp"

unsigned int Thing::is_dead() const
{
  return m_dead;
}

void Thing::kill(unsigned int level)
{
  m_dead = level;
}

void Thing::resurrect()
{
  m_dead = 0;
}

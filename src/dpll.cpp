/*! \file  */ 
#include "../include/dpll.hpp"

enum class apparition {TRUE, FALSE, UNKNOWN, UNDEF};

// Some colors
string color_blue = "\".5 .5 .5\"";
string color_red = "red";
string color_yellow = "yellow";


/** ******************************************************************************
 * \brief Constructeur du Dpll.
 Construit l'instance de DPLL avec les arguments donnés.
 * \param p_v : nombre de varibles du problème
 * \param p_formules : formules du problème à résoudre
 * \param p_WL : activation ou non de la méthode des *Watched Litterals*
 * \param p_cl : activation ou non du *Clause Learning*
 * \param p_graph : activation ou non la génération du graphe de résolution, ainsi que le mode interactif (note : si vrai, alors *cl* est automatiquement passé à vrai aussi.
 */
Dpll::Dpll(unsigned int p_v,
           std::vector<Clause> p_formules,
           bool p_WL,
           bool p_cl,
           bool p_graph,
           bool p_TH,
           Theory* p_theory,
           bool p_RAND,
           bool p_MOMS,
           bool p_DLIS,
           bool p_VSIDS,
           int p_VSIDS_COUNT,
	int p_VERBOSITY):
        v(p_v),
        nc_clause(0),
        level(2),
        to_bt(0),
        formules(p_formules),
        WL(p_WL),
        cl(p_cl),
        graph(p_graph),
        TH(p_TH),
        theory(p_theory),
        RAND(p_RAND),
        MOMS(p_MOMS),
        DLIS(p_DLIS),
        VSIDS(p_VSIDS),
        VSIDS_COUNT(p_VSIDS_COUNT),
	VERBOSITY(p_VERBOSITY)
{
	

	nb_conflicts = 0;
	nb_decisions = 0;
	nb_cllearned = 0;
	if(graph) // force cl if graph
		cl = true;

	c = formules.size();
	solution = std::vector<Litteral>(p_v+1);
	justifications = std::vector<int>(2*p_v+1, -1);

#ifdef DEBUG
	std::cout << "nombre de variables : " << p_v << std::endl;
#endif
        
	// initialiser solution[i] à L(i), state::unknown
	for(unsigned int i = 1; i < 1+p_v; i++)
	{
		solution[i].state = states::UNKNOWN;
		solution[i].var = i;
		solution[i].resurrect();
	}


	/**
	 * Priorité des modes de choix: RAND > MOMS > DLIS > VSIDS
	 */
	RAND = RAND;
	MOMS = MOMS && (!RAND);
	DLIS = DLIS && (!RAND) && (!MOMS);
	VSIDS = VSIDS && (!RAND) && (!MOMS) && (!DLIS);

#ifdef DEBUG
	std::cout << "Mode de choix:";
	if(RAND)
		std::cout << "RAND" << std::endl;
	else if(MOMS)
		std::cout << "MOMS" << std::endl;
	else if(DLIS)
		std::cout << "DLIS" << std::endl;
	else if(VSIDS)
		std::cout << "VSIDS" << std::endl;
	else
		std::cout << "Normal" << std::endl;
#endif

	if(VSIDS)
		vsids_score = std::vector<unsigned int>(p_v+1, 0);
}



/** ******************************************************************************
 * \brief Résoud le problème avec les paramètres donnés au constructeur.
 */
std::vector<Litteral> Dpll::solve()
{
	if(c == 0)
		return solution;
	level = 2; // 2 sinon quand on backtrack y'a des trucs morts à 0 donc vivant
	int i = 1;
	int res = 0;
#ifdef DEBUG
	std::cout << "Clauses dpll : \n";
	for(Clause clause : formules)
		std::cout << clause << std::endl;
#endif  
	if(WL)
	{
		for(Clause clause : formules)
		{
			if(clause.is_dead() == 0)
			{
				int size = clause.size();
				if(size == 0)
					return no_sol;
				else if(size == 1)
				{
					Litteral L = clause.get(0);
					if(L.is_dead() == 0)
					{
#ifdef DEBUG
						std::cout << "KILLING : " << L << std::endl;
#endif
						unsigned int lvl1 = 2;
						if(solution[L.var].state == states::UNKNOWN)
						{
							solution[L.var] = L;
							solution[L.var].kill(lvl1);
							if(!assume(L))
								return no_sol;
						}
						else if(solution[L.var] == -L)
						{
							return no_sol;
						}
			
					}
				}
			}
		}
	} // no WL ///////////////////////////////////


	while(true)
	{
#ifdef DEBUG
		std::cout << "etat des formules : \n";
		for(Clause clause : formules)
			std::cout << clause << std::endl;
		std::cout << "etat des solutions : \n";
		for(int i = 1; i < solution.size(); i++)
			std::cout << solution[i] << " ";
		std::cout << "\n";
#endif
            
		if(!bcp())
			// on a rencontré une contradiction à la propagation
		{
			if((cl || graph) && !choix.empty())
			{
				nb_cllearned++;
				to_bt = learn();
			}
			else
			{
				to_bt = level-1;
			}
                    
			// Mode interactif
			if(graph){
				std::cout << std::endl << "Contrainte détectée, que faire ?" << std::endl;
				std::cout << "g : enregistrer le graphe dans 'tmp.dot'" << std::endl;
				std::cout << "c : continuer jusqu'au prochain conflit (choix par défaut)" << std::endl;
				std::cout << "t : terminer l'execution en quittant le mode interactif" << std::endl;
				std::cout << "?> ";
				std::string userin;
				std::cin >> userin;

				if(userin == "g"){
					graph_drawer->store("tmp.dot");
					std::cout << "Graphe enregistré dans 'tmp.dot'" << std::endl;
				}else if(userin == "t")
					graph = false;
				else{}
			}
			nb_conflicts++;

			if(!backtrack())
			{
				// on a renconté une insatisfiabilité (on a dépilé tous les choix)
				solution = no_sol;//return no_sol;
				break;
			}

		}
		else
		{
			std::stack<Litteral> empty_stack;
			propagations.swap(empty_stack);
			graph_drawer->clear();
#ifdef DEBUG
			std::cout << "graph is now EMPTY\n";
#endif
			// il faut faire un choix
			if(!choose())
				// on n'a pas pu faire de choix : on a donc tout
				// assigné (choose gère le changement vers no_sol en
				// cas de contradiction)
			{
				break;
			}
			else
			{
				nb_decisions++;
			}
		}

	}
	if(VERBOSITY > 0)
	{
		std::cout << "\t======= STATISTICS ======\n";
		std::cout << "\tconflicts: " << nb_conflicts << std::endl;
		std::cout << "\tdecisions: " << nb_decisions << std::endl;
		if(cl)
			std::cout << "\tlearned: " << nb_cllearned << std::endl;
	}
	return solution;
}
  

/** ******************************************************************************
 * \brief fait la propagation des contraintes, jusqu'à ce que tout soit stabilité
 * \return true si l'on ne rencontre pas de contradiction, false sinon.
 */
bool Dpll::bcp()
{
#ifdef DEBUG
	std::cout << "bcp()" << std::endl;;
#endif
	int res = cons_prop();
#ifdef DEBUG
	std::cout << "cons_prop()1" << std::endl;
#endif
	while(res == 1)
	{
		res = cons_prop();
	}
        
#ifdef DEBUG
	std::cout << "fin de la cons_prop. Resultat " << res << std::endl;
#endif
        
	return (res == 0);
}



/** ******************************************************************************
 * \brief Fait la propagation de contraintes de l'algo
 * 
 * \return 1 si une nouvelle contrainte a été détectée, 0 si aucune contrainte n'a été détectée et -1 si une contradiction a été soulevée
 */
int Dpll::cons_prop()
{
	if(WL)
	{
		int res = 0;
		if(to_prop.empty() && !(formules[formules.size()-1].size() == 1 && formules.size() > c))
		{
			c = formules.size();
#ifdef DEBUG
			std::cout << "to_prop is EMPTY\n";
#endif
			return res;
		}
		else
		{
			if(formules[formules.size()-1].size() == 1 && formules.size() > c)
			{
#ifdef DEBUG
				std::cout << "détection clause unitaire apprise" << std::endl;
#endif
				Litteral l = formules[formules.size()-1].get(0);
				c = formules.size();
				if(l.state == states::TRUE)
					justifications[l.var] = formules.size()-1;
				else
					justifications[v+l.var] = formules.size()-1;
				l.kill(2);
				solution[l.var] = l;
				if(!assume(l))
					return -1;
				else
					return 1;
			}

			Litteral L = to_prop.back();
#ifdef DEBUG
			std::cout << "to_prop.pop() " << L << std::endl;
#endif
			to_prop.pop_back();
			if(choix.empty() || L.var != choix.top().var)
				propagations.push(L);
			solution[L.var] = L;
			solution[L.var].kill(level);
#ifdef DEBUG
			std::cout << "propagations.push " << L << std::endl;
			std::cout << "assume " << L << std::endl;
#endif
			if(!assume(L))
				return -1;
			else
				return 1;
		}

                
	}
	else
	{ // Pas WL ///////////////////////////
		int al_size = 0;
#ifdef DEBUG
		std::cout << "cons_prop at level " << level << std::endl;
#endif

		//Test de la cohérence de la théorie avant tout
		if(TH && !theory->check_validity()){
#ifdef DEBUG
			std::cout << "Théorie non valide !" << std::endl;
#endif
                
			return -1;
		}
        
		std::vector<apparition> occurences((1+v), apparition::UNDEF);
	    
		for(unsigned int i = 0; i < formules.size(); i++)
		{
			if(formules[i].is_dead() == 0)
			{
				al_size = formules[i].alive_size();
				if(al_size == 0)
				{
#ifdef DEBUG
					std::cout << "al_size == 0 in " << formules[i] << std::endl;
#endif
					return -1;
				}
				for(Litteral L : formules[i])
				{
					/**
					 * Deux cas de présentent :
					 * 1) la formule est une clause unitaire, on a une nouvelle contrainte qu'on propage
					 * (le 2) est plus bas)
					 */
					if(L.is_dead() == 0)
					{
						if(al_size == 1) 
						{
							if(graph){
#ifdef DEBUG
								std::cout << "\t\t adding blue vertex " << L.var << std::endl;
#endif
								graph_drawer->add_vertex(L.var, color_blue, "filled");
                                        
								for(Litteral K : formules[i]){ // Pour tous les littéraux de la clause
									if(K!=L){
										if(K.is_dead() == level){ // Niveau actuel: bleu
#ifdef DEBUG
											std::cout << "\t\tdrawing (in blue) " << K.var << " linked to " << L.var << std::endl;
#endif
											graph_drawer->add_vertex(K.var, color_blue, "filled");
											graph_drawer->add_edge(K.var, L.var);
										}
										else
										{                    // Autre niveau: blanc
#ifdef DEBUG
											std::cout << "\t\tdrawing (default color) " << K.var << " linked to " << L.var << std::endl;
#endif
											graph_drawer->add_vertex(K.var);
											graph_drawer->add_edge(K.var, L.var);
										}
									}
								}
							}
				
				
							solution[L.var] = L;
							solution[L.var].kill(level);
							if(L.state == states::TRUE)
								justifications[L.var] = i;
							else
								justifications[v+L.var] = i;
							propagations.push(L);
#ifdef DEBUG
							std::cout << "justifications[" << L.var << "] = " << i << "\n";
							std::cout << "propagations.push(" << L << ")\n";
#endif
							if(assume(L))
							{
#ifdef DEBUG
								std::cout << "end of cons_prop" << std::endl;
#endif
								return 1;
							}
							else
							{
								return -1;
							}
						}
						/** 2) sinon, on compte les occurences de chaque
						 * variable, pour espérer en trouver avec une seule
						 * polarité 
						 * Le principe est le suivant : on tient à jour
						 * un tableau d'occurences des variables, avec pour
						 * états UKNOWN (on n'a pas encore vu la variable), TRUE
						 * (on a vu seulement la variable dans son état vrai),
						 * FALSE (idem avec l'état faux), et UNDEF : on a déjà
						 * vu les deux polarités
						 */
						else if(!TH) // polarité + SMT -> pas bon
						{
							switch(L.state)
							{
							case states::TRUE:
							{
								if(occurences[L.var] == apparition::FALSE)
									occurences[L.var] = apparition::UNKNOWN;
								else if(occurences[L.var] == apparition::UNDEF)
									occurences[L.var] = apparition::TRUE;
							}
							case states::FALSE:
							{
								if(occurences[L.var] == apparition::TRUE)
									occurences[L.var] = apparition::UNKNOWN;
								else if(occurences[L.var] == apparition::UNDEF)
									occurences[L.var] = apparition::FALSE;
							}		    
							}
						}
					}
				}
			}
		}
		if(!TH) // polarité + SMT = fail
		{
			/** Ici on fait la propagation des assignations de variables : si on
			 * trouve une variable non encore assignée dans la solution, et
			 * qu'on sait qu'elle n'apparait qu'avec une seule polarité, on la
			 * fixe
			 */
			for(unsigned int i = 1; i < occurences.size(); i++)
			{
				// si la variable a pas encore été assignée et qu'il faut. On gère deux cas
				if(occurences[i] == apparition::TRUE && solution[i].state == states::UNKNOWN) // si que vraie
				{
					Litteral l(i);
					l.state = states::TRUE;
					solution[i] = l;
					solution[i].kill(level); // on sait ainsi quand est-ce qu'on en a déduit cette contrainte
#ifdef DEBUG
					std::cout << "prop par polarité sur " << l << std::endl;
#endif                    
					if(assume(l)) // vérifier qu'il n'y a pas de conflit avec le kill, et qu'il n'y a aucune clause vide
						return 1;
					else
						return -1;
				}
				else if(occurences[i] == apparition::FALSE && solution[i].state == states::UNKNOWN) // si que fausse
				{
					Litteral l(i);
					l.state = states::FALSE;
					solution[i] = l;
					solution[i].kill(level);
                    
#ifdef DEBUG
					std::cout << "prop par polarité sur " << l << std::endl;
#endif                    
					if(assume(l))
						return 1;
					else
						return -1;
				}
			}
		}
	}
	return 0;
}


/** ******************************************************************************
 * \brief Fait un assume sur les formules du problème
 * \return true si tout va bien
 * \return false si une contradiction est détectée
 */
bool Dpll::assume(Litteral is_true)
{
	if(is_true.state == states::UNKNOWN)
	{
		return true;
	}
#ifdef DEBUG
	std::cout << "ASSUMING " << is_true << " (level : " << level << ")" << std::endl;
#endif

	// Modulo théorie
	if(TH)
	{
#ifdef DEBUG
		std::cout << "Appel du solveur dans la théorie..." << std::endl;
#endif
		Clause resu = theory->add_assignation(is_true, level);


		// Si la clause n'est pas vide; erreur
		if(resu.size() != 0)
		{
#ifdef DEBUG
			std::cout << " Contradiction dans la théorie !" << std::endl;
#endif
			if(VERBOSITY > 1)
				std::cout << "\tContradiction detected in the SMT-solver" << std::endl;
			return false;
		}
#ifdef DEBUG
		std::cout << "ok" << std::endl;
#endif
	}
    
    
	// Si pas de problème, assume le litteral
	bool res = true;
	for(int i = 0; i < formules.size(); i++)
	{
		if(formules[i].is_dead() == 0)
			if(((WL && (formules[i].get_WL1().var == is_true.var || formules[i].get_WL2().var == is_true.var)) || (!WL)) && !formules[i].assume(is_true, level, v, solution, i, justifications, propagations, to_prop))
			{
				res = false;
				nc_clause = i;
              
				if(VERBOSITY > 1)
				{
					std::cout << "\tContradiction in ";
					verbosedisplay(std::cout, formules[i]);
					std::cout << " (while assuming ";
					verbosedisplay(std::cout, is_true);
					std::cout << ")" << std::endl;
				}
#ifdef DEBUG
				std::cout << "\t\tCONTRADICTION sur : ";
				std::cout << formules[i] << res << std::endl;
#endif
              
				if(graph){
					graph_drawer->add_vertex("CONTR", color_red, "filled");
                              
					for(Litteral K : formules[i]){ // Pour tous les littéraux de la clause
						if(K.is_dead() == level){ // Niveau actuel: bleu
#ifdef DEBUG
							std::cout << "\t\tgraph blue CONTR : " << K.var << std::endl;
#endif
							graph_drawer->add_vertex(K.var, color_blue, "filled");
							graph_drawer->add_edge(K.var, "CONTR");
						}else{                    // Autre niveau: blanc
#ifdef DEBUG
							std::cout << "\t\tgraph default color CONTR : " << K.var << std::endl;
#endif
							graph_drawer->add_vertex(K.var);
							graph_drawer->add_edge(K.var, "CONTR");
						}
                                      
					}
				}
                      
				break;
			}
	}
#ifdef DEBUG
	std::cout << "end of assume\n";
#endif    
  
	return res;
}


/** ******************************************************************************
 * \brief Fait le prochain choix pour continuer l'algo dpll
 * \return true ssi un choix a été fait (sinon, il faut renvoyer la solution
 */
bool Dpll::choose()
{
	level++;
	int i = 1;
	int res;
	if(RAND) // RAND CHOICE
	{
#ifdef DEBUG
		std::cout << "CHOIX rand : ";
#endif
		int count = 0;
		for(int j = 1; j <= v; j++)
			if(solution[j].state == states::UNKNOWN)
				count++;
		if(count == 0) // on a fini, tout est assigné
		{
			if(!bcp())
				solution = no_sol;
			return false;
		}
		else
		{
			std::default_random_engine generator;
			std::uniform_int_distribution<int> vardistr(1,count);
			std::uniform_int_distribution<int> choicedistr(0, 1);
			int c = vardistr(generator);
			i = 1;
			while(c > 1)
			{
				while(solution[i].state != states::UNKNOWN)
					i++;
				c--;
			}
			while(solution[i].state != states::UNKNOWN)
				i++;
			solution[i].isfc = true;
			solution[i].kill(level);
#ifdef DEBUG
			std::cout << i;
#endif
			if(choicedistr(generator) == 0)
			{ // on assigne vrai
#ifdef DEBUG
				std::cout << " true\n";
#endif
				solution[i].state = states::TRUE;
			}
			else
			{ // on assigne faux
#ifdef DEBUG
				std::cout << " false\n";
#endif
				solution[i].state = states::FALSE;
			}
		}
	}
	else if(MOMS)
	{
		unsigned int min_alive = INT_MAX;
		for(Clause c: formules)
			if(c.is_dead() == 0 && c.size() < min_alive)
				min_alive = c.size();
		vector<int> occu_moms(1+v, 0); // occurence de chacun des littéraux dans la clause min
		for(Clause c: formules)
			if(c.is_dead() == 0 && c.size() == min_alive)
				for(Litteral L: c)
					if(solution[L.var].state == states::UNKNOWN)
					{
						occu_moms[L.var] += 1;
					}
		unsigned int maxi = 0;
		i = 0;
		for(int j = 1; j < occu_moms.size(); j++)
			if(occu_moms[j] > maxi)
			{
				maxi = occu_moms[j];
				i = j;
			}
#ifdef DEBUG
		std::cout << "(moms)CHOIX : " << i << std::endl;
		std::cout << "(max) " << maxi << std::endl;
#endif
		if(maxi == 0)
		{
#ifdef DEBUG
			std::cout << "Pas de choix valide avec MOMS. On tente naïvement" << std::endl;
#endif
			i = 0;
			while(i < 1+v && (solution[i].state != states::UNKNOWN)) 
				// on continue tant qu'on trouve pas de variable inconnue
				i++;
		
			if(i == 1+v) // on a fini, tout est assigné, sans soucis
			{
				if(!bcp())
				{
					solution = no_sol;
				}
				return false;
			}
			else
			{
#ifdef DEBUG
				std::cout << "CHOIX : " << i << " true\n";
#endif
				solution[i].state = states::TRUE;
				solution[i].kill(level);
				solution[i].isfc = true;
			}
		}
		solution[i].state = states::TRUE;
		solution[i].kill(level);
		solution[i].isfc = true;
	}
	else if(DLIS) //// DLIS
	{
		std::vector<unsigned int> count = std::vector<unsigned int>(v+1);

		for(int j=1; j<v+1; j++){
			if(solution[j].state == states::UNKNOWN)
			{
				for(Clause &c : formules){
					for(Litteral l : c){
						if(!l.is_dead() && (l.var == j))
							count[j]++;
					}
				}
			}
		}

		unsigned int max = 0;
		unsigned int max_indice = 0;
		for(unsigned int j=0; j<v+1; j++)
		{
			if(count[j]>max){
				max = count[j];
				max_indice = j;
			}
		}

		i = max_indice;

		if(i == 0){ // Aucun littéral ne peut satisfaire de clause
			if(!bcp())
			{
				solution = no_sol;
			}
			return false;
		}

#ifdef DEBUG
		std::cout << "CHOIX DLIS: " << i << " true" << std::endl;
#endif

		solution[i].state = states::TRUE;
		solution[i].isfc = true; // première tentative
		solution[i].kill(level);

            
	}
	else if(VSIDS) //// VSIDS
	{
		unsigned int max = 0;
		unsigned int max_indice = 0;
		for(unsigned int j=0; j<v+1; j++)
		{
			if(solution[j].state == states::UNKNOWN // solution pas choisie 
			   && vsids_score[j]>=max){ // score mieux
				max = vsids_score[j];
				max_indice = j;
			}
		}

		i = max_indice;

		if(i==0){ // rien choisi: tout est assigné dans la solution
			if(!bcp())
			{
				solution = no_sol;
			}
			return false;
		}

		solution[i].state = states::TRUE;
		solution[i].kill(level);
		solution[i].isfc = true;
            
	}
	else  //// NORMAL
	{
		while(i < 1+v && (solution[i].state != states::UNKNOWN)) 
			// on continue tant qu'on trouve pas de variable inconnue
			i++;
    
		if(i == 1+v) // on a fini, tout est assigné, sans soucis
		{
			if(!bcp())
			{
				solution = no_sol;
			}
			return false;
		}
		else
		{
#ifdef DEBUG
			std::cout << "CHOIX : " << i << " true\n";
#endif
			solution[i].state = states::TRUE;
			solution[i].kill(level);
			solution[i].isfc = true;
		}
	}
	
	if(VERBOSITY > 1)
		std::cout << "\tBetting on " << i << std::endl;

	// propagation du littéral choisi
	if(!WL)
		assume(solution[i]);
	else
		to_prop.push_front(solution[i]);
    
	choix.push(solution[i]);
	choix.top().kill(level);
	return true;
}


/** ******************************************************************************
 * \brief fait le backtrack jusqu'à l'assignation posant problème
 * \return true ssi on a pu backtracker (sinon, on est sur un problème non satisfiable)
 */
bool Dpll::backtrack()
{
#ifdef DEBUG
	std::cout << "Backtracking ; to_bt = " << to_bt << std::endl;
#endif
	if(choix.empty())
	{
		return false;
	}
	if(graph)
	{
		std::cout << "Backtrack au niveau " << to_bt << ", on annule donc le choix sur " << choix.top().var << std::endl;
	}
#ifdef DEBUG
	std::cout << "Backtracking " << choix.top() << std::endl;
#endif

	if(cl || graph) // soit on fait du clause learning et on sait jusqu'ou remonter
	{
		if(to_bt < 2)
			to_bt = 2;
		while(!choix.empty() && choix.top().is_dead() > to_bt){
#ifdef DEBUG
	    
			std::cout << "(cl) " << choix.top() << std::endl;
#endif
			choix.pop();
		}
	}
	else  // No CL
	{
		while(!choix.empty() && choix.top().isfc == false)
		{
#ifdef DEBUG
			std::cout << "isfc false" << choix.top() << std::endl;
#endif
			choix.pop();
		}
	}
        
	if(!choix.empty())
		level = choix.top().is_dead();
	else
		level = 2;

	int bt = max((unsigned int) 2, level-1);

	// Backtrack de la théorie
	if(TH)
	{
#ifdef DEBUG
		std::cout << "backtrack in the theory" << std::endl;
#endif
		theory->backtrack_to(bt);
	}
    
	// backtrack des clauses
	for(Clause& clause : formules)
	{
		clause.backtrack_to(bt);
	}

	// Backtrack des solutions
	for(int i = 1; i < 1+v; i++)
	{
		if(solution[i].is_dead() > bt)
		{
			if(solution[i].state == states::TRUE)
				justifications[i] = -1;
			else
				justifications[v+i] = -1;
			solution[i].state = states::UNKNOWN;
			if(cl || WL)
				solution[i].resurrect();
#ifdef DEBUG
			std::cout << "Après : " << solution[i] << "\t justifications[" << i << "] = -1\n";
#endif
		}
	}
    
	if(!cl && !choix.empty())
	{
		choix.top().state = states::FALSE; // Dernier choix passe à false
		choix.top().isfc = false;
	}
	// CL
	std::list<Litteral> empty_list;
	if(!choix.empty())
	{
		empty_list.push_front(choix.top());
#ifdef DEBUG
		std::cout << choix.top() << " in the new to_prop\n";
#endif
	}
	else if(cl && formules[formules.size()-1].size() == 1)
	{
		Litteral l = formules[formules.size()-1].get(0);
		empty_list.push_front(l);
#ifdef DEBUG
		std::cout << l << " in the new to_prop\n";
#endif
	}
	to_prop.swap(empty_list);

	// Clause learning pour WL
	if(cl && WL)
	{ // on veut juste faire gaffe que les WL de la clause apprise sont ok
		Clause t = formules[formules.size()-1];
		Litteral wl1 = t.get_WL1();
		Litteral wl2 = t.get_WL2();
		if(solution[wl1.var].state != states::UNKNOWN && solution[wl1.var] == -wl1)
		{
//	    std::cout << "wl1 mal placé\n";
			formules[formules.size()-1].assume(solution[wl1.var], level, v, solution, formules.size()-1, justifications, propagations, to_prop);
		}
		else if(solution[wl2.var].state != states::UNKNOWN && solution[wl2.var] == -wl2)
		{
//	    std::cout << "wl2 mal placé\n";
			formules[formules.size()-1].assume(solution[wl2.var], level, v, solution, formules.size()-1, justifications, propagations, to_prop);
		}
#ifdef DEBUG
		std::cout << "Mise en place des WL dans la dernière clause : " << formules[formules.size()-1] << std::endl;
		std::cout << "to_prop  = ";
		for(auto it = to_prop.begin(); it != to_prop.end(); ++it)
		{
			std::cout << *it << " ";
		}
		std::cout << std::endl;
#endif
		if(t.size() == 1)
		{
			std::list<Litteral> empty_list;
			to_prop.swap(empty_list);
		}
		
	}

    
	if(!WL && !cl && !choix.empty())
		assume(choix.top()); // Propage le choix
    
	if(!cl && !choix.empty())
	{
		solution[choix.top().var].state = states::FALSE; // MAJ la solution
		justifications[choix.top().var] = -1;
#ifdef DEBUG
		std::cout << "justifications[" << choix.top().var << "] = -1\n";
#endif
	}

	if(!cl && choix.empty())
		return false;
	return true;
}


/** ******************************************************************************
 * \brief Fait toute la partie de clause learning
 * \return Le niveau auquel il faut backtracker
 */
unsigned int Dpll::learn()
{
	Clause t = formules[nc_clause];
#ifdef DEBUG
	std::cout << "\tLEARNING FROM " << t << "\n\tt.nb_new(level) = " << t.nb_new(level,solution) << "\n";
#endif
	Litteral last_choice = choix.top();
	Litteral w1, w2;
	Litteral to_find; // Contient le bleu
	int j = 0;
	while(t.nb_new(level, solution) > 1) // tant qu'il y a au moins deux points bleus, on cherche des mariages
	{
#ifdef DEBUG
		std::cout << "t.nb_new " << t.nb_new(level, solution) << std::endl;
#endif
		j = 0;
		to_find = propagations.top();
		while(!t.is_in(to_find.var) && !propagations.empty())
		{
#ifdef DEBUG
			std::cout << "propagations.top() " << to_find << std::endl;
#endif
			propagations.pop();
			to_find = propagations.top();
		}

#ifdef DEBUG
		std::cout << "to_find=" << to_find<< std::endl;
#endif
		if(to_find.state == states::TRUE)
		{
			j = justifications[to_find.var];
		}
		else
		{
			j = justifications[v+to_find.var];
		}


#ifdef DEBUG
		std::cout << "j = " << j << std::endl;
		std::cout << "\tresolving " << t << " and ";
		std::cout << formules[j] << " wrt ";
		std::cout << to_find.var << "\n";
		std::cout << "ok\n";
#endif
		t = resolve(to_find.var, t, formules[j], WL, solution); // ici il faut marier. 

#ifdef DEBUG
		std::cout << "resolved : " << t << std::endl;
#endif

	}
	if(graph)
	{
		std::cout << "Clause apprise " << t << std::endl;
	}


	// Niveau de backtrack
	unsigned int to_backtrack = 0;
	unsigned int llvl; // Niveau actuel (bouge)
	Litteral uip;
	for(Litteral L : t)
	{
		llvl = solution[L.var].is_dead();
        
#ifdef DEBUG
		std::cout <<"Searching to_backtrack : " << to_backtrack << " " << llvl << " " << L << std::endl;
#endif

		if(llvl == level){ // Le dernier bleu
#ifdef DEBUG
			cout << "UIP FOUND " << L <<", level: " << level << "  llvl: " << llvl << std::endl;
#endif
                
			uip = L;
			if(graph)
				graph_drawer->add_vertex(uip.var, color_yellow, "filled", true);
		}
		else if(llvl > to_backtrack)
		{
			to_backtrack = llvl;
		}
	}

	if(WL)
		t.add_WL(solution, to_prop, to_backtrack);
    
	formules.push_back(t);

	if(VERBOSITY > 1)
	{
		std::cout << "\tLearned: ";
		verbosedisplay(std::cout, t);
		std::cout << std::endl;
	}

#ifdef DEBUG
	std::cout << "Learned : " << t << std::endl;
#endif

	// si VSIDS, il faut mettre à jour les scores.
	if(VSIDS){
		for(Litteral l : t)
		{
			vsids_score[l.var]++;
		}

		vsids_count = (vsids_count+1) % VSIDS_COUNT; // tous les V_C learn()
		if(vsids_count == 0){
			for(unsigned int& j: vsids_score){
				j /= 2; // on divise tout par 2. 
			}
		}
	}
    

	if(to_backtrack == 0)
	{
#ifdef DEBUG
		std::cout << "CAS ETRANGE\n";
#endif
		return 2;//level-1; // je sais pas si ce cas est censé ce produire.
	}
	else
		return to_backtrack;
}

#include "../../include/theories/congruence.hpp"

Term::Term(string name, list<Term> args) : name(name), subterms(args)
{
}

std::ostream &operator<<(std::ostream &os, Term const &T)
{
        os << T.name;

        if(!T.subterms.empty())
                os << "(";
        
        for(Term t : T.subterms){
                os << t;
        }
        
        if(!T.subterms.empty())
                os << ")";
        
        return os;
}


Congruence::Congruence() 
{
}

#include "../../include/theories/difference.hpp"

Difference::Difference()
{
    number = 1;
}

Difference::~Difference()
{
}


/**
 * \brief Ajoute une inégalité en prenant en argument cette inégalité. Est censé \^etre appelé seulement par la fonction add_ineq suivante, qui s'occupe de tout vérifier
 */
void Difference::add_ineq(Ineq i)
{
    donnees.push_front(i);
    int gs = graphe.size();
    int m = std::max(std::max(std::max(gs, (i.x1+1)), (i.x2+1)), (int)(number+1));
    graphe.resize(m);
}


/**
 * \brief Ajoute une inégalité en prenant l'inégalité sous la forme item1 - item2 <= cst (cette forme est créé par la transformation de tseitin de la théorie des différences
 * \return la variable du littéral correspondant à cette inégalité
 */
int Difference::add_ineq(int item1, int item2, int cst)
{
    number++;
#ifdef DEBUG
    std::cout << "add_ineq " << item1 << " - " << item2 << " <= " << cst << std::endl;
#endif
    Ineq i;
    Litteral l((unsigned int)number);
    i.litt = l;
    i.x1 = item1;
    i.x2 = item2;
    i.c = cst;
    for(auto it = donnees.begin(); it != donnees.end(); ++it)
    {
	    if(it->x1 == i.x1 && it->x2 == i.x2 && it->c == i.c)
	    {
#ifdef DEBUG
		    std::cout <<"\tthis is" << it->litt << " " << it->x1 << " " << it->x2 << " " << it->c << std::endl;
#endif
		    return it->litt.var;
	    }
    }
#ifdef DEBUG
    std::cout << "added with val " << number << std::endl;
#endif
    add_ineq(i);
    return i.litt.var;
}

void Difference::backtrack_to(unsigned int level)
{
    for(int u = 1; u < graphe.size(); u++)
    {
	auto v = graphe[u].begin();
	while(v != graphe[u].end())
	{
	    if(v->is_dead() > level)
		graphe[u].erase(v++);
	    else
		v++;
	}
    }
}

int Difference::get_number()
{
    return number;
}

/**
 * \brief Met à jour l'état du littéral suivant ce qui est passé en argument, et vérifie que cela n'engendre pas de contradiction
 * \return une clause vide si tout va bien, une clause exprimant la contradiction sinon
 */
Clause Difference::add_assignation(Litteral is_true, unsigned int level)
{
#ifdef DEBUG
   std::cout << "add_assignation " << is_true << std::endl;
#endif
    Ineq ine;
    ine.x1 = -1;
    ine.x2 = -1;
    for(auto i = donnees.begin(); i != donnees.end(); ++i)
    {
	if((*i).litt.var == is_true.var)
	{
	    ine.x1 = (*i).x1;
	    ine.x2 = (*i).x2;
	    ine.litt = (*i).litt;
	    ine.c = (*i).c;
	    break;
	}
    }
    int source = -1;
    if(ine.x1 == -1 && ine.x2 == -1)
    {
	return Clause();
    }
    else if(is_true.state == states::TRUE)
    { // on crée dans le graphe une arr^ete x1 - x2 <= c
	Arete a;
	a.dest = ine.x2;
	a.poids = ine.c;
	a.kill(level);
	graphe[ine.x1].push_back(a);
	source = ine.x1;
    }
    else
    { // on crée dans le graphe une arrete x1 - x2 > c ce qui veut dire x2 - x1 <= -c -1
	Arete a;
	a.dest = ine.x1;
	a.poids = -(1+ine.c);
	a.kill(level);
	source = ine.x2;
	graphe[ine.x2].push_back(a);
    }
    return check_neg_cycle(source);
}
	

bool operator==(const Ineq& i1, const Ineq& i2)
{
    return (i1.litt == i2.litt) && (i1.x1 == i2.x1) && (i1.x2 == i2.x2) && (i1.c == i2.c);
}

bool operator!=(const Ineq& i1, const Ineq& i2)
{
    return !(i1 == i2);
}


/**
 * \brief Vérifie que la composante connexe du graphe contenant source n'a pas de cycle de poids négatif
 * \return Clause vide s'il n'y a pas de cycle de poids négatif, une clause expliquant la contradiction au SAT-solver sinon
 */
Clause Difference::check_neg_cycle(int source)
{
#ifdef DEBUG
    std::cout << "check_neg_cycle" << std::endl;
#endif
    d.resize(graphe.size()+1);
    for(int i = 0; i < graphe.size(); i++)
    {
	d[i] = INT_MAX;
    }
    d[source] = 0;
    for(int i = 1; i < graphe.size(); i++)
    {
	for(int u = 0; u < graphe.size(); u++)
	{
	    for(auto a = graphe[u].begin(); a != graphe[u].end(); ++a)
	    {
		if(d[u] != INT_MAX && d[a->dest] > d[u] + a->poids)
		{
		    d[a->dest] = d[u] + a->poids;
		    peres[a->dest] = u;
		}
	    }
	}
    }
    for(int u = 1; u < graphe.size(); u++)
    {
	for(auto a = graphe[u].begin(); a != graphe[u].end(); ++a)
	{
	    if(d[u] != INT_MAX && d[a->dest] > d[u] + a->poids)
	    {
#ifdef DEBUG
		std::cout << "Cycle négatif trouvé" << std::endl;
#endif
		Clause c;
		c.add_litt(a->dest);
		int f = u;//a->dest;
		while(f != a->dest)
		{
			c.add_litt(Litteral(-f));
			f = peres[f];
		}
		return c;
	    }
	}
    }
#ifdef DEBUG
		std::cout << "Pas de cycle négatif" << std::endl;
#endif
		return Clause();
}


/**
 * \brief Vérifie que tout le graphe ne contient pas de cycle de poids négatif
 * \return True ssi le graphe ne contient pas de cycle de poids négatif
 */
bool Difference::check_validity()
{
    peres.resize(graphe.size()+1);
    for(int i = 0; i < peres.size(); i++)
    {
	peres[i] = -1;
    }
    for(int i = 1; i < peres.size(); i++)
	if(peres[i] == -1)
		if(check_neg_cycle(i).size() > 0)
			return false;
    return true;
}

bool compIneqLitt(const Ineq& i1, const Ineq& i2)
{
    return i1.litt.var < i2.litt.var;
}


void Difference::print_sol()
{
    Arete a;
    a.poids = 0;
    for(int i = 1; i < graphe.size(); i++)
    {
	a.dest = i;
	graphe[0].push_back(a);
    }
    check_neg_cycle(0);
    for(int i = 1; i < graphe.size(); i++)
    {
	std::cout << "x" << i << " = " << -d[i] << std::endl;
    }
#ifdef DEBUG
    std::cout << "printing done" << std::endl;
#endif
}

#include "../../include/theories/equality.hpp"


Equality::Equality()
{
}

Equality::~Equality()
{
}

/**
 * Ajoute une égalité en prenant en argument un littéral.
 * ATTENTION: aucune vérification n'est faite sur la nouveauté ou non du littéral de l'Eq.
 */
void Equality::add_eq(Eq eq)
{
        assignations.push_back(eq);
        uf.make_set(eq.item1);
        uf.make_set(eq.item2);
}

/*
 * Ajoute une égalité en prenant les deux variables égales.
 \return la variable du littéral correspondant à cette nouvelle égalité
 Il faut bien penser à ajouter TOUS les littéraux avant de commencer à donner des assignations.
 */
int Equality::add_eq(std::string item1, std::string item2)
{
    if(item2 < item1)
	return add_eq(item2, item1);
    
    #ifdef DEBUG
    std::cout << "add_eq " << item1 << "=" << item2  << " with number = " << number << std::endl;
    #endif
    
        Eq new_eq;
        new_eq.item1 = item1;
        new_eq.item2 = item2;
        new_eq.litt = Litteral((unsigned int)number);
	for(auto it = assignations.begin(); it != assignations.end(); it++)//Eq e : assignations)
	    if((*it).item1 == new_eq.item1 && (*it).item2 == new_eq.item2)
	    {
                    #ifdef DEBUG
                    std::cout << "\tthis is " << (*it).litt.var << " " << (*it).item1 << " " << (*it).item2 << std::endl;
                    #endif
                    
		return (*it).litt.var;
	    }
        number++;

        add_eq(new_eq);
        
        #ifdef DEBUG
	std::cout << "\tthis is new : " << new_eq.litt.var << std::endl;
        #endif
        
        return new_eq.litt.var;
}

int Equality::get_number()
{
    return number;
}


void Equality::backtrack_to(unsigned int level)
{
        #ifdef DEBUG
        std:: cout << "Etat AVANT backtrack:" << std::endl;
        print_sol();
        #endif
        
        for(Eq& e : assignations){
                if(e.litt.is_dead() > level){
                        e.litt.resurrect();
                        e.litt.state = states::UNKNOWN;
                }
        }

        uf.backtrack_to(level);

        #ifdef DEBUG
        std:: cout << "Etat APRES backtrack:" << std::endl;
        print_sol();
        #endif
}

/**
 * Met à jour l'état du littéral comme état l'état de is_true, puis vérifie que ce nouvel état est cohérent.
 ATTENTION, ne pas ajouter d'assignation à des étapes inférieures sans backtracker avant, les états ne seraient pas correctemnt mis à jour.
 ATTENTION, toutes les égalites doivent avoir été ajoutées avant de faire ça.
 \return [] La clause vide si tout va bien
 \return Clause exprimant le problème s'il y a une contradiction
 */
Clause Equality::add_assignation(Litteral is_true, unsigned int level)
{
        #ifdef DEBUG
        std::cout << "\t add_assignation:" << is_true << ", level " << level << std::endl;
        #endif
        
        // Mise à jour de l'état du littéral
        for(Eq& e : assignations){
                if(e.litt.var == is_true.var){ // C'est de ce littéral là qu'on parle !
                        e.litt.state = is_true.state;
                        e.litt.kill(level);

                        // Si l'état est vrai, il faut fusionner les classes
                        if(e.litt.state == states::TRUE)
                        {
                                uf.union_set(e.item1, e.item2, level);
                        }
                        
                        break;
                }
        }

        #ifdef DEBUG
        std::cout << "\t result: " << check_neqs() << std::endl;
        #endif
        
        return check_neqs();
}

/**
 * \brief Teste la validité de la théorie actuelle
 * \return true si tout va bien
 * \return false si contradiction
 */
bool Equality::check_validity()
{
        return (check_neqs().size() == 0);
}

/**
 * \brief Affiche les classes d'équivalence à l'écran.
 */

#include <unordered_set>
#include <iostream>

void Equality::print_sol()
{
        // Construction des classes
        std::map< std::string, std::unordered_set<std::string> > classe;

        for(Eq& e: assignations)
        {
                classe[ uf.find_set(e.item1) ].insert(e.item1);
                classe[ uf.find_set(e.item2) ].insert(e.item2);
        }

        for(auto& C : classe){
                for(std::string item : C.second){
                        std::cout << item << " = ";
                }
                std::cout << std::endl;
        }
}



/**
 * Vérifie si la théorie actuelle est cohérente
 * \return [] la clause vide si tout va bien
 * \return Clause exprimant le conflir si une contradiction est détectée avec les inégalité et les classes actuelles d'union-find.
 * Pour le moment retourn juste non (inégalité), mais à voir si c'est correct..
 */
Clause Equality::check_neqs()
{
        for(Eq &eq : assignations)
        {
                if(eq.litt.state == states::FALSE){ // Inégalité
                        std::string set1 = uf.find_set(eq.item1);
                        std::string set2 = uf.find_set(eq.item2);
                        if(set1 == set2){
                                // L'inégalité est fausse
                                Clause c;
                                // inégalité
                                c.add_litt(- eq.litt);
                                return c;
                        }
                }
        }

        return Clause();
}



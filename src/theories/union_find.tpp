
template<typename T>
UnionFind<T>::UnionFind()
{
        rank_map.insert(typename std::map<unsigned int, rank_t>::value_type(0, rank_t() ));
        
        parent_map.insert(typename std::map<unsigned int, parent_t>::value_type(0, parent_t()));
        
        rank_pmaps.insert(typename std::map<unsigned int, boost::associative_property_map<rank_t> >::value_type(0, boost::associative_property_map<rank_t> (rank_map[0]) ));
        
        parent_pmaps.insert(typename std::map<unsigned int, boost::associative_property_map<parent_t> >::value_type(0, boost::associative_property_map<parent_t> (parent_map[0]) ));
        
        dsets.insert(typename std::map<unsigned int, dset_type* >::value_type(0, new dset_type (rank_pmaps[0], parent_pmaps[0])));
        
}

template<typename T>
void UnionFind<T>::make_set(const T& item)
{
        dsets[0]->make_set(item);
}

template<typename T>
void UnionFind<T>::union_set(const T& item1, const T& item2, unsigned int level)
{
        if(rank_map.find(level) == rank_map.end()) // rank_map[level] n'existe pas !
        {
                
                rank_map.insert(typename std::map<unsigned int, rank_t>::value_type(level, rank_map[last_level] ));
        
                parent_map.insert(typename std::map<unsigned int, parent_t>::value_type(level, parent_map[last_level] ));

                rank_pmaps.insert(typename std::map<unsigned int, boost::associative_property_map<rank_t> >::value_type(level, boost::associative_property_map<rank_t> (rank_map[level]) ));
        
        parent_pmaps.insert(typename std::map<unsigned int, boost::associative_property_map<parent_t> >::value_type(level, boost::associative_property_map<parent_t> (parent_map[level]) ));
        
        dsets.insert(typename std::map<unsigned int, dset_type* >::value_type(level, new dset_type (rank_pmaps[level], parent_pmaps[level])));
                
                if(level>last_level)
                        last_level = level;
        }

        dsets[level]->union_set(item1, item2);
}


template<typename T>
T UnionFind<T>::find_set(T item, unsigned int level)
{
        return dsets[level]->find_set(item);
}

template<typename T>
T UnionFind<T>::find_set(T item)
{
        return find_set(item, last_level);
}

template<typename T>
void UnionFind<T>::backtrack_to(unsigned int level)
{
        unsigned int i;
        for(i=last_level; i>level; i--)
        {
                if(rank_map.find(i) != rank_map.end()) // rank_map[i] existe
                {
                        rank_map.erase(i);
                        parent_map.erase(i);
                        rank_pmaps.erase(i);
                        parent_pmaps.erase(i);
                        //delete dsets[i];
                        dsets.erase(i);
                }
        }

        // Trouver le prochain rang existant
        while( dsets.find(i) == dsets.end() )// dsets[i] n'existe pas
        {
                i--;
        }
        
        last_level = i;
}

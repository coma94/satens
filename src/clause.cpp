#include "../include/clause.hpp"

Clause::Clause() : m_WL(false)
{
        this->resurrect();
}
Clause::Clause(bool WL) : m_WL(WL)
{
        this->resurrect();
}

void Clause::add_litt(Litteral L)
{
        m_litt.push_back(L);

        if(m_WL) // Si WL, il faut assigner les littéraux surveillés: les 2 premiers
        {
                if(WL_w1 == -1)
                        WL_w1 = 0;
                else if(WL_w2 == -1)
                        WL_w2 = 1;
        }             
}
        
bool Clause::is_in(unsigned int var) const
{
        for(Litteral L : m_litt){
                if(L.var == var)
                        return true;
        }

        return false;
}

bool Clause::is_in(Litteral L) const
{
        for(Litteral l : m_litt){
                if(l == L)
                        return true;
        }

        return false;
}

unsigned int Clause::nb_new(unsigned int level, std::vector<Litteral>& WL_solution) const
{
	unsigned int count = 0;
	for(Litteral l : m_litt)
	{
		if(WL_solution[l.var].is_dead() == level)
			count++;
	}

	return count;
}


bool Clause::is_empty() const
{
	for(Litteral L : m_litt)
		if(!L.is_dead())
			return false;
	return true;	
}

bool Clause::assume(Litteral is_true, unsigned int level)
{
        std::vector<Litteral> solution_bidon;
	std::vector<int> justif_bidon;
	std::stack<Litteral> prop_bidon;
	std::list<Litteral> todo_bidon;
        return assume(is_true, level, 0, solution_bidon, 0, justif_bidon, prop_bidon, todo_bidon);
}

bool Clause::assume(Litteral is_true, unsigned int level, std::vector<Litteral>& WL_solution, int numero_clause, std::vector<int>& justifications, std::stack<Litteral>& propagations)
{
	std::list<Litteral> todo_bidon;
	return assume(is_true, level, 0, WL_solution, 0, justifications, propagations, todo_bidon);
}

bool Clause::assume(Litteral is_true, unsigned int level, unsigned int p_v, std::vector<Litteral>& WL_solution, int numero_clause, std::vector<int>& justifications, std::stack<Litteral>& propagations, std::list<Litteral>& to_prop)
{
	bool ret = false;

	if(m_WL) // avec WL
	{
#ifdef DEBUG
		std::cout << "assuming " << is_true << " in " << *this << "\t";
#endif

		if(size() == 0) // Clause vide
			return false;

		if(size() == 1 && m_litt[WL_w1].var == is_true.var)
		{ // Clause unitaire
#ifdef DEBUG
			std::cout << "Clause unitaire détectée : " << m_litt[WL_w1] << std::endl;
#endif
			if(m_litt[WL_w1] == is_true)
			{
				return true;
			}
			if(WL_solution[m_litt[WL_w1].var].state == states::UNKNOWN)
			{ // Il était inconnu
#ifdef DEBUG
				std::cout << "Clause unitaire inconnue" << std::endl;
#endif
				if(!to_prop.empty())
				{
					auto it = to_prop.cbegin();
					for(it = to_prop.cbegin(); it != to_prop.cend(); ++it)
					{
						if((*it).var == m_litt[WL_w1].var)
							break;
					}
					if(*it == m_litt[WL_w1])
					{
#ifdef DEBUG
						std::cout << "assignation de " << m_litt[WL_w1] << " déjà prévue\n";
#endif
						this->kill(level);
						return true;
					}
				}
				else
				{
					to_prop.push_front(m_litt[WL_w1]);
					if(m_litt[WL_w1].state == states::TRUE)
						justifications[m_litt[WL_w1].var] = numero_clause;
					else
						justifications[p_v+m_litt[WL_w1].var] = numero_clause;
#ifdef DEBUG
					std::cout << "to_prop.push(" << m_litt[WL_w1] << ")\n";
#endif
					this->kill(level);
					return true;                
				}
			}
			else
			{ // Déjà connu
#ifdef DEBUG
				std::cout << "Debut else traitement unitaire\n";
#endif                        
				if(m_litt[WL_w1].state == is_true.state){ // Même signe: ok pas de soucis
#ifdef DEBUG
					std::cout << "Pas de soucis\n";
#endif
					m_done = true;
					return true;
				}else{ // signe opposé: contradicton
					m_done = true;
					return false;
				}
			}
		}
		else
		{ // Clause avec plus de 2 littéraux
                
			int* to_change = NULL;
			int* other = NULL;
                
			if(WL_w1 != -1 && (is_true == m_litt[WL_w1] || is_true == -m_litt[WL_w1]))
			{
				to_change = &WL_w1;
				other = &WL_w2;
			}
			else if(WL_w2 != -1 && (is_true == m_litt[WL_w2] || is_true == -m_litt[WL_w2]))
			{
				to_change = &WL_w2;
				other = &WL_w1;
			}

                
			if(to_change == NULL) // Le changement ne concerne pas les WL
			{
#ifdef DEBUG
				std::cout << "Pas de changement " << m_litt[WL_w1] << " " << m_litt[WL_w2] << "\n";
#endif
				return true;
			}
			else
			{
				// Même signe: toute la clause est vraie
				if(m_litt[*to_change] == is_true){
#ifdef DEBUG
					std::cout << "WL déjà assigné à vrai\n";
#endif
					if(this->is_dead() == 0)
						this->kill(level);
					m_done = true;
					return true;
				}
				// signe opposé : on tue le littéral et on en cherche un autre
				// ?? structure: ça ne vaudrait pas le coup de trier de manière à ne pas avoir à reparcourir pour trouver un nouveau
				else
				{
					m_litt[*to_change].kill(level);
                                
					bool found = false;
					for(int i=0; i<m_litt.size(); i++)
					{
						Litteral& L = m_litt[i];
						if((i != (*other)) && (i != (*to_change)))
						{
							if(L == WL_solution[L.var])
							{ // Vrai dans la solution
#ifdef DEBUG
								std::cout << "on a trouvé un vrai\n";
#endif
								this->kill(level);
								(*to_change) = i;
								m_done = true;
								return true;
							}
							else if (L == -WL_solution[L.var])
							{ // Faux dans la solution, on le tue
								L.kill(WL_solution[L.var].is_dead());//level);
							}
							else
							{ // Ni l'un ni l'autre, on le selectionne
								found = true;
								(*to_change) = i;
								break;
							}
						}
					}

                                
					if(found)
					{ // Si on a trouvé un nouveau littéral : pas de problème
#ifdef DEBUG
						std::cout << "nouveau litteral trouve\n";
#endif
						return true;
					}
					else
					{    // Si impossible, on essaye d'assigner le littéral restant 

						if(WL_solution[m_litt[*other].var].state == states::UNKNOWN)
						{ // La variable était encore inconnue
#ifdef DEBUG
							std::cout << "nouvelle assignation à faire" << m_litt[*other] << ", numero_clause = " << numero_clause << "\n";
#endif
							auto it = to_prop.cbegin();
							for(it = to_prop.cbegin(); it != to_prop.cend(); ++it)
							{
								if((*it).var == m_litt[*other].var)
									break;
							}
							if(*it == m_litt[*other])
							{
#ifdef DEBUG
								std::cout << "assignation déjà prévue\n";
#endif
								return true;
							}
							else
							{
								to_prop.push_front(m_litt[*other]);
								if(m_litt[*other].state == states::TRUE)
									justifications[m_litt[*other].var] = numero_clause;
								else
									justifications[p_v+m_litt[*other].var] = numero_clause;
								return true;
							}
						}
						else
						{
							if(WL_solution[m_litt[*other].var].state == m_litt[*other].state)
							{
#ifdef DEBUG
								std::cout << "assignation déjà faite\n";
#endif
								// Le littéral restant est déjà à vrai dans la solution
								if(this->is_dead() == 0)
									this->kill(level);
								return true;
							}
							else
							{ // Assignations opposées.. Impossible
#ifdef DEBUG
								std::cout << "assignation contradictoire sur " << m_litt[*other] << "\n";
#endif
								m_done = true;
								return false;
							}
						}
					}
				}
			}
			free(to_change);
			free(other);
		}/**/
	}
	else
	{ // sans WL
		for(Litteral& L : m_litt)
		{
			if(L == is_true)
			{          // Si le littéral est présent.. 
				this->kill(level); // toute la clause est satisfaite, donc inutile
				ret = true;
				break;
			}
			else if(L == (-is_true))
			{   // Sinon..
				L.kill(level);     // On supprime les littéraux opposés
			}
			else
			{
				if(! L.is_dead())   // Encore vivant ?
					ret = true; // Il reste un littéral non supprimé
			}
		}
	}

	return ret;
}

void Clause::backtrack_to(unsigned int level)
{
        if(this->is_dead() > level)
                this->resurrect();

        if(m_WL) // avec WL
        {
                // On ne met à jour que les 2 surveillés
                if(WL_w1 != -1){
                        if(m_litt[WL_w1].is_dead() > level)
                                m_litt[WL_w1].resurrect();
                }
                if(WL_w2 != -1){
                        if(m_litt[WL_w2].is_dead() > level)
                                m_litt[WL_w2].resurrect();
                }
        }else{ // sans WL
                for(Litteral& L : m_litt){
                        if(L.is_dead() > level)
                                L.resurrect();
                }
        }
}

unsigned int Clause::alive_size() const
{
	unsigned int nvivants = 0;
	if(this->is_dead())
		return 0;
	for(Litteral L : m_litt){
		if(L.is_dead() == 0)
			nvivants++;
	}
	return nvivants;
}

unsigned int Clause::size() const
{
	return m_litt.size();
}

Litteral Clause::get_WL1() const
{
	return m_litt[WL_w1];
}

Litteral Clause::get_WL2() const
{
	return m_litt[WL_w2];
}

// iterators
std::vector<Litteral>::const_iterator Clause::begin() const
{
        return m_litt.begin();
}

std::vector<Litteral>::const_iterator Clause::end() const
{
        return m_litt.end();
}


Clause resolve(unsigned int to_avoid, const Clause& c1, const Clause& c2, bool WL, std::vector<Litteral>& solution)
{
	Clause res;
	Litteral l1, l2;
    
	int i = 0, j = 0;
	Litteral backup;
	while(i < c1.size() && j < c2.size())
	{
		l1 = c1.m_litt[i];
        
		l2 = c2.m_litt[j];
#ifdef DEBUG
		std::cout << l1 << " " << l2 << "\n";
#endif
        
		if(l1.var != to_avoid && l2.var != to_avoid)
		{
			if(l1 > l2)
			{
				if(solution[l1.var].is_dead() != 2) // ça veut dire que c'est pas une contrainte forte
					res.m_litt.push_back(l1);
				else
					backup = l1;
                
#ifdef DEBUG
				std::cout << "pushed back l1\n";
#endif
                
				i++;
			}
			else if(l2 > l1)
			{
				if(solution[l2.var].is_dead() != 2)
					res.m_litt.push_back(l2);
				else
					backup = l2;
                
#ifdef DEBUG
				std::cout << "pushed back l2\n";
#endif
                
				j++;
			}	   
			else if(l1 == l2)
			{
				if(solution[l2.var].is_dead() != 2)
					res.m_litt.push_back(l2);
				else
					backup = l2;
                
#ifdef DEBUG
				std::cout << "duplication. pushed back one\n";
#endif
                
				j++;
				i++;
			}

		}
		if(l1.var == to_avoid)
			i++;
		if(l2.var == to_avoid)
			j++;
	}
	while(i < c1.size())
	{
		l1 = c1.m_litt[i];
        
#ifdef DEBUG
		std::cout << l1 << "\n";
#endif
        
		if(l1.var != to_avoid)
		{
                
#ifdef DEBUG
			std::cout << "pushed back\n";
#endif
			if(solution[l1.var].is_dead() != 2)
				res.m_litt.push_back(l1);
			else
				backup = l1;
		}
		i++;
	}
	while(j < c2.size())
	{
		l2 = c2.m_litt[j];
        
#ifdef DEBUG
		std::cout << l2 << "\n";
#endif
        
		if(l2.var != to_avoid)
		{
                
#ifdef DEBUG
			std::cout << "pushed back\n";
#endif
			if(solution[l2.var].is_dead() != 2)
				res.m_litt.push_back(l2);
			else 
				backup = l2;
		}
		j++;
	}
	if(res.size() == 0)
	{
		res.m_litt.push_back(backup);
	}
	return res;
}

Litteral Clause::get(unsigned int pos)
{
	return m_litt[pos];
}

void Clause::add_WL(const std::vector<Litteral>& solution, std::list<Litteral>& to_prop, unsigned int to_backtrack)
{
#ifdef DEBUG
	std::cout << "searching for watched litterals...\n";
	std::cout << "Knowing that : ";
	if(solution.size() > 0)
	{
		for(Litteral l : solution)
			std::cout << l << " ";
		std::cout << std::endl;
	}
	else
	{
		std::cout << "NO DATA !" << std::endl;
	}
#endif
	m_WL = true;
	WL_w1 = -1;
	WL_w2 = -1;
	for(int i = 0; i < m_litt.size(); i++)
	{
		if(solution[m_litt[i].var].state == states::UNKNOWN || solution[m_litt[i].var] == m_litt[i] || solution[m_litt[i].var].is_dead() > to_backtrack)
		{
			WL_w1 = i;
			break;
		}
	}
	if(WL_w1 == -1)
	{
#ifdef DEBUG
		std::cout << "In resolve, WL_w1 not found :(\n";
#endif
		WL_w1 = 0;
	}
	for(int i = WL_w1+1; i < m_litt.size(); i++)
	{
		if(solution[m_litt[i].var].state == states::UNKNOWN || solution[m_litt[i].var] == m_litt[i] || solution[m_litt[i].var].is_dead() > to_backtrack)
		{
			WL_w2 = i;
			break;
		}
	}
	if(WL_w2 == -1)
	{
#ifdef DEBUG
		std::cout << "In resolve, WL_w2 not found :(\n";
#endif
		WL_w2 = (WL_w1+1) % m_litt.size();
	}
#ifdef DEBUG
	std::cout << "w1, w2 = " << WL_w1 << ", " << WL_w2 << "\n";
	std::cout << "END OF SEARCH\n";
#endif
}


std::ostream &operator<<(std::ostream &os, Clause const &C)
{
        if(C.is_dead())
                os << "[";

        for(int i=0; i<C.m_litt.size(); i++){
                os << C.m_litt[i];
                if(i == C.WL_w1 || i== C.WL_w2)
                        os << "w";
                
                os << " ";
        }

        if(C.is_dead())
                os << "]";

        return os;
}

std::ostream& verbosedisplay(std::ostream& os, Clause const &C)
{
	for(int i = 0; i < C.m_litt.size(); i++)
	{
		verbosedisplay(os, C.m_litt[i]);
		os << " ";
	}
	return os;
}

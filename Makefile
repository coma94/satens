CXX=g++
CPPFLAGS=-lboost_program_options -std=c++11 -O3
DEBUGFLAGS=-g -D DEBUG
RM=rm -rf
DOC=doxygen

PTBUILD=build
PTTEST=testing

INCLSRCS=src/litteral.cpp src/thing.cpp
INCLOBJ=$(PTBUILD)/litteral.o $(PTBUILD)/thing.o

SRCS=src/main.cpp  src/dpll.cpp src/clause.cpp src/graph.cpp src/theory.cpp src/theories/congruence.cpp src/theories/equality.cpp src/theories/difference.cpp src/parse_eq.cpp src/expr.cpp src/parse_bool.cpp src/parse_ineg.cpp
OBJS=$(PTBUILD)/main.o $(PTBUILD)/dpll.o $(PTBUILD)/clause.o $(PTBUILD)/graph.o $(PTBUILD)/theory.o $(PTBUILD)/congruence.o $(PTBUILD)/equality.o $(PTBUILD)/difference.o $(PTBUILD)/parse_ineg.o $(PTBUILD)/expr.o $(PTBUILD)/parse_bool.o $(PTBUILD)/parse_eq.o
OBJDEBUG=$(PTBUILD)/maindebug.o $(PTBUILD)/dplldebug.o $(PTBUILD)/clausedebug.o $(PTBUILD)/graphdebug.o $(PTBUILD)/theorydebug.o $(PTBUILD)/congruencedebug.o $(PTBUILD)/equalitydebug.o $(PTBUILD)/differencedebug.o $(PTBUILD)/parse_ineg.o $(PTBUILD)/expr.o $(PTBUILD)/parse_bool.o $(PTBUILD)/parse_eq.o 

OBJTESTS=$(PTBUILD)/tests.o $(PTBUILD)/dplldebug.o $(PTBUILD)/clausedebug.o $(PTBUILD)/graphdebug.o 

SRCGENR=$(PTTEST)/gensat.cpp
OBJGENR=$(PTBUILD)/gensat.o

SRCGENB=$(PTTEST)/genrbmodel.cpp
OBJGENB=$(PTBUILD)/genrbmodel.o

OBJDOC=Doxyfile

DISTFILES=$(INCLSRCS) $(SRCS) $(SRCGENR) $(SRCGENB) $(SRCWL) $(OBJDOC) readme.tex ref.bib readme.pdf include/clause.hpp include/litteral.hpp include/dpll.hpp include/thing.hpp include/graph.hpp Makefile readme.pdf tests/*.cnf testing/bench.py testing/bench3.py testing/checker.py testing/gensat.cpp testing/graph.py testing/graph_rand.py testing/mksat.py testing/cnf/cbs/* doc/* doc/*/* doc/*/*/* testing/resultats/*/*  include/*.hpp include/theories/*.hpp src/theories/union_find.tpp tests/*.tst tests/*.df tests/*.eq testing/resultats/* Exps/*

dir_guard=@mkdir -p $(@D)

all: solver

readme: readme.tex ref.bib
	pdflatex readme.tex
	bibtex readme.aux
	pdflatex readme.tex
	pdflatex readme.tex
	$(RM) readme.aux
	$(RM) readme.log
	$(RM) readme.out
	$(RM) readme.toc

dist: $(DISTFILES) 
	zip rendu_Monat_Moutot.zip $(DISTFILES)	

gen : gen_rand gen_modelb

gen_rand: $(OBJGENR)
	$(CXX) -o testing/gen_rand $(OBJGENR) $(CPPFLAGS)

gen_modelb: $(OBJGENB)
	$(CXX) -o testing/gen_modelrb $(OBJGENB) $(CPPFLAGS)

solver: $(OBJS) $(INCLOBJ)
	$(CXX) -o resol $(OBJS) $(INCLOBJ) $(CPPFLAGS)

debug: $(OBJDEBUG) $(INCLOBJ)
	$(CXX) -o resol_debug $(OBJDEBUG) $(INCLOBJ) $(CPPFLAGS) 

doc:	*
	doxygen Doxyfile

$(PTBUILD)/gensat.o: $(PTTEST)/gensat.cpp
	@$(dir_guard)
	$(CXX) -c $< -o $@ $(CPPFLAGS)

$(PTBUILD)/genrbmodel.o: $(PTTEST)/genrbmodel.cpp
	@$(dir_guard)
	$(CXX) -c $< -o $@ $(CPPFLAGS)

$(PTBUILD)/maindebug.o: src/main.cpp include/dpll.hpp src/expr.cpp src/parse_ineg.cpp src/parse_bool.cpp src/parse_eq.cpp include/expr.hpp include/parse_ineg.hpp include/parse_bool.hpp include/parse_eq.hpp
	@$(dir_guard)
	$(CXX) $(DEBUGFLAGS) -c $< -o $@ $(CPPFLAGS)

$(PTBUILD)/dplldebug.o: src/dpll.cpp include/clause.hpp
	@$(dir_guard)
	$(CXX) $(DEBUGFLAGS) -c $< -o $@ $(CPPFLAGS)

$(PTBUILD)/clausedebug.o: src/clause.cpp include/thing.hpp include/litteral.hpp
	@$(dir_guard)
	$(CXX) $(DEBUGFLAGS) -c $< -o $@  $(CPPFLAGS)

$(PTBUILD)/graphdebug.o: src/graph.cpp include/graph.hpp
	@$(dir_guard)
	$(CXX) $(DEBUGFLAGS) -c $< -o $@ $(CPPFLAGS)

$(PTBUILD)/theorydebug.o: src/theory.cpp include/theory.hpp
	@$(dir_guard)
	$(CXX) $(DEBUGFLAGS) -c $< -o $@ $(CPPFLAGS)

$(PTBUILD)/congruencedebug.o: src/theories/congruence.cpp include/theories/congruence.hpp
	@$(dir_guard)
	$(CXX) $(DEBUGFLAGS) -c $< -o $@ $(CPPFLAGS)

$(PTBUILD)/equalitydebug.o: src/theories/equality.cpp include/theories/equality.hpp
	@$(dir_guard)
	$(CXX) $(DEBUGFLAGS) -c $< -o $@ $(CPPFLAGS)

$(PTBUILD)/differencedebug.o: src/theories/difference.cpp include/theories/difference.hpp
	@$(dir_guard)
	$(CXX) $(DEBUGFLAGS) -c $< -o $@ $(CPPFLAGS)


$(PTBUILD)/main.o: src/main.cpp include/dpll.hpp src/expr.cpp src/parse_ineg.cpp src/parse_bool.cpp src/parse_eq.cpp
	@$(dir_guard)
	$(CXX) -c $< -o $@  $(CPPFLAGS)

$(PTBUILD)/dpll.o: src/dpll.cpp include/clause.hpp
	@$(dir_guard)
	$(CXX) -c $< -o $@ $(CPPFLAGS)

$(PTBUILD)/expr.o: src/expr.cpp include/expr.hpp
	@$(dir_guard)
	$(CXX) -c $< -o $@ $(CPPFLAGS)

$(PTBUILD)/parse_ineg.o: src/parse_ineg.cpp include/parse_ineg.hpp src/expr.cpp src/theories/difference.cpp
	@$(dir_guard)
	$(CXX) -c $< -o $@ $(CPPFLAGS)

$(PTBUILD)/parse_bool.o: src/parse_bool.cpp include/parse_bool.hpp src/expr.cpp
	@$(dir_guard)
	$(CXX) -c $< -o $@ $(CPPFLAGS)

$(PTBUILD)/parse_eq.o: src/parse_eq.cpp include/parse_eq.hpp src/expr.cpp src/theories/equality.cpp
	@$(dir_guard)
	$(CXX) -c $< -o $@ $(CPPFLAGS)

$(PTBUILD)/clause.o: src/clause.cpp include/thing.hpp include/litteral.hpp
	@$(dir_guard)
	$(CXX) -c $< -o $@ $(CPPFLAGS)

$(PTBUILD)/litteral.o: src/litteral.cpp include/thing.hpp
	@$(dir_guard)
	$(CXX) -c $< -o $@ $(CPPFLAGS)

$(PTBUILD)/thing.o: src/thing.cpp 
	@$(dir_guard)
	$(CXX) -c $< -o $@ $(CPPFLAGS)

$(PTBUILD)/graph.o: src/graph.cpp include/graph.hpp
	@$(dir_guard)
	$(CXX) -c $< -o $@ $(CPPFLAGS)

$(PTBUILD)/theory.o: src/theory.cpp include/theory.hpp
	@$(dir_guard)
	$(CXX) -c $< -o $@ $(CPPFLAGS)

$(PTBUILD)/congruence.o: src/theories/congruence.cpp include/theories/congruence.hpp src/theory.cpp include/theory.hpp
	@$(dir_guard)
	$(CXX) -c $< -o $@ $(CPPFLAGS)

$(PTBUILD)/equality.o: src/theories/equality.cpp include/theories/equality.hpp src/theory.cpp include/theory.hpp
	@$(dir_guard)
	$(CXX) -c $< -o $@ $(CPPFLAGS)

$(PTBUILD)/difference.o: src/theories/difference.cpp include/theories/difference.hpp src/theory.cpp include/theory.hpp
	@$(dir_guard)
	$(CXX) -c $< -o $@ $(CPPFLAGS)


.PHONY: clean dist-clean

clean:
	$(RM) $(PTBUILD)

dist-clean: clean
	$(RM) rendu_Monat_Moutot.zip
	$(RM) resol
	$(RM) resol-wl
	$(RM) resol_debug
	$(RM) resol_tests
	$(RM) $(PTTEST)/gen_rand

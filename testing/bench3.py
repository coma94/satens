# -*- coding: utf-8 -*-
from bench import bench
import sys

import numpy as np
import matplotlib.pyplot as plt

if __name__ == '__main__':
    if(len(sys.argv) < 3):
        print "Usage : " + sys.argv[0] + " files.cnf"
        sys.exit(1)
    fig = plt.figure()
    cnffiles = sys.argv[1:]
    ax = fig.add_subplot(111)
    ax.set_ylabel("temps (s)")
    ax.set_xlabel("numero de fichier")
    fig.subplots_adjust(top=0.85)
    
    data_minisat = []
    data_resol = []
    data_resol_wl = []
    data_resol_cl = []
    data_resol_wl_cl = []
    
    for x in cnffiles:
        try:
            print "minisat:  " + x
            data_minisat.append(bench(["minisat"]+[x]))
            print "resol:    " + x
            data_resol.append(bench(["../resol"]+[x]))
            print "resol-wl: " + x
            data_resol_wl.append(bench(["../resol"]+["--wl"]+[x]))
            print "resol+cl:    " + x
            data_resol_cl.append(bench(["../resol"]+["--cl"]+[x]))
            #print "resol-wl+cl: " + x
            #data_resol_wl_cl.append(bench(["../resol"]+["--cl"]+["--wl"]+[x]+["WL"]))
            
        except KeyboardInterrupt:
            print("KeyboardInterrupt !")
            break

    plt.plot(data_minisat, 'k', label="minisat")
    plt.plot(data_resol, 'r', label="resol")
    plt.plot(data_resol_wl, 'm', label="resol-wl")
    plt.plot(data_resol_cl, 'b', label="resol+cl")
    #plt.plot(data_resol_wl_cl, 'g', label="resol-wl+cl")
        
    plt.legend()
    plt.show()

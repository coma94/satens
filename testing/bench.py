# -*- coding: utf-8 -*-
from timeit import timeit
import subprocess
import sys

def bench(cmd_params, reps=1):
    devnull = open('/dev/null', 'w')
    time_taken = timeit(stmt = "subprocess.call(%s, stdout=devnull, stderr=devnull)" % cmd_params, setup = """import subprocess; devnull = open('/tmp/bla', 'w');""", number = reps) / reps
    return time_taken

if __name__ == '__main__':
    if(len(sys.argv) < 3):
        print "Usage : " + sys.argv[0] + " sat_solver files.cnf"
        sys.exit(1)
        
    cnffiles = sys.argv[2:]
    solver = sys.argv[1].split()
    for x in cnffiles:
        print x + ", " + str(bench(solver+[x]))



# But : lancer python bench.py cmd bla.csv *.cnf et avoir dans fichier dans cnf, la moyenne de temps (sur n itérations) cmd ...cnf pour chaque fichier. Puis peut ^etre crééer une fonction qui génère directement les *.cnf ensuite ?

# -*- coding: utf-8 -*-
from bench import bench
import sys

import numpy as np
import matplotlib.pyplot as plt

from multiprocessing import Process, Queue

nbtests = 5

# Fonction de chaque thread : fait 5 tests et retourne la moyenne et l'écart dans la Queue
def process_bench(solver, filee, queue):
    times = []
    # On fait les tests
    for i in range(nbtests):
        times.append( bench(solver.split()+[filee]) )

    moyenne = sum(times)/len(times)
    
    ecartmax = 0
    for time in times:
        ecart = abs(time - moyenne)
        if(ecart > ecartmax):
            ecartmax = ecart
    
    queue.put(  ( solver, moyenne, ecartmax)  )


if __name__ == '__main__':
    # Arguments...
    if(len(sys.argv) < 3):
        print "Usage : " + sys.argv[0] + " solver1.. solverk -files file1.cnf .. filek.cnf "
        sys.exit(1)
    fig = plt.figure()

    solvers = []
    cnffiles= []

    # On parse les arguments (attention à l'ordre !)
    files_passed = False
    for elem in sys.argv[1:]:
        if(elem=="-files" or elem=="--files"):
            files_passed = True
        elif(not files_passed):
            solvers.append(elem)
        else:
            cnffiles.append(elem)
            
    cnffiles.sort()

    # Axes du graph
    ax = fig.add_subplot(111)
    ax.set_ylabel("temps (s)")
    ax.set_xlabel("numero de fichier")
    fig.subplots_adjust(top=0.85)

    
    color=['b', 'g', 'r', 'c', 'm', 'y', 'k'] # Les couleurs des courbes

    
    datas = {}
    errors = {}

    for solver in solvers:
        datas[solver] = []
        errors[solver] = []

    #gen data
    # Pour chaque fichier...
    for x in cnffiles:
        try:
            q = Queue()
            procs = []

            #start process
            for solver in solvers:
                procs.append(Process( target=process_bench, args= (solver, x, q) ))
                procs[-1].start()

            #join process
            for p in procs:
                p.join()

            # get res
            while not q.empty():
                (solver, time, ecart) = q.get()
                datas[solver].append(time)
                errors[solver].append(ecart)
                
            
        except KeyboardInterrupt:
            print("KeyboardInterrupt !")
            break

    # print data
    for i,solver in enumerate(solvers):
#        plt.plot(datas[solver], color[i % 7], label=solver)
        plt.errorbar(range(len(datas[solver])), datas[solver], yerr=errors[solver], color=color[i%7], label=solver)
        
    plt.legend()
    plt.show()

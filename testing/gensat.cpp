#include <iostream>
#include <string>
#include <random>

int main(int nargs, char** args)
{
  if(nargs < 3)
    {
      std::cout << "Usage : " << args[0] << " nb_var nb_clauses\n";
      return 2;
    }
  int v = std::stoi(args[1]);
  int c = std::stoi(args[2]);
  // on va générer c clauses de taille 3, ayant des variables comprises entre 1 et v
  std::cout << "p cnf " << v << " " << c << std::endl;
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_int_distribution<> dis(1, v);
  std::uniform_int_distribution<> maxi(3, v);
  std::uniform_int_distribution<> sig(0, 1);
  int r = 0, s = 0;
  int m = 0;
  for(int i = 0; i < c; i++)
  {
      m = maxi(gen);
      for(int j = 0; j < m; j++)
      {
	  s = sig(gen);
	  if(s == 0)
	    std::cout << "-" << dis(gen) << " ";
	  else
	    std::cout << dis(gen) << " ";
	}
      std::cout << "0\n";
    }
  return 0;
}

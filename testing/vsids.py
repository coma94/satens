# -*- coding: utf-8 -*-
from bench import bench
import sys

import numpy as np

from multiprocessing import Process, Queue

from math import sqrt

def process_bench(solver, filee, queue):
    queue.put(  ( solver, bench(solver.split()+[filee]) )  )


if __name__ == '__main__':
    if(len(sys.argv) < 3):
        print "Usage : " + sys.argv[0] + " file1.cnf .. file2.cnf "
        sys.exit(1)

    solvers = []
    cnffiles= []

    files_passed = False
    for elem in sys.argv[1:]:
        cnffiles.append(elem)
            
    
    datas = {}

    for i in range(1,50):
        solvers.append("../resol --wl --cl --vsids-count "+str(i))

    
    for solver in solvers:
        datas[solver] = []

    #gen data
    for x in cnffiles:
        try:
            q = Queue()
            procs = []

            #start process
            for solver in solvers:
                procs.append(Process( target=process_bench, args= (solver,x, q) ))
                procs[-1].start()

            #join process
            for p in procs:
                p.join()

            # get res
            while not q.empty():
                (solver, time) = q.get()
                datas[solver].append(time)
            
            
        except KeyboardInterrupt:
            print("KeyboardInterrupt !")
            break
        
        
    #print results    
    means = {}
    
    for solver in solvers:
        mean = 0
        qmean = 0
        nb = 0
        for i in datas[solver]:
            mean += i
            qmean += i*i
            nb += 1

        mean = mean / nb
        qmean = sqrt(qmean)
        
        means[solver] = mean
        
        print solver, " mean =", mean, ", Qmean = ", qmean

# -*- coding: utf-8 -*-
# Vérifie que le sat_solver passé en argument donne une assignation
# correcte au problème file.cnf supposé satisfiable a. Pour cela, on
# ajoute les assignations en contraintes dans le solver, et on demande
# à un solveur "de confiance" de vérifier la satisfiabilité"
import sys
import os
import shutil
import subprocess

def is_int(s):
    if(s[0] == '-'):
        return s[1:].isdigit()
    else:
        return s.isdigit()

def check(cmd, safe_solver, param):
    print "Checking " + param,
    tmp = "/tmp/test"+str(os.getpid())+".cnf"
    devnull = open('/dev/null', 'w')
    shutil.copy(param, tmp)
    ftmp = open(tmp, 'a')
    res = subprocess.check_output(cmd.split()+[param], stderr=devnull)
    for x in res.split():
        if x == "UNSATISFIABLE":
            try:
                res_sol = subprocess.check_output([safe_solver, tmp], stderr=devnull)
            except subprocess.CalledProcessError as e:
                res_sol = e.output
            if(res_sol.split()[-1] != "UNSATISFIABLE"):
                print "Error !"
                sys.exit(1)
            else:
                print "...Ok"
                return True

    
    res = res.split("SATISFIABLE\n")[-1]
    for x in res.split():
        ftmp.write(x + ' 0\n')
    ftmp.close()
    try:
        res_sol = subprocess.check_output([safe_solver, tmp], stderr=devnull)
    except subprocess.CalledProcessError as e:
        res_sol = e.output
    if(res_sol.split()[-1] != "SATISFIABLE"):
        print "Error !"
        sys.exit(1)
    print "...Ok"
    return True


if __name__ == '__main__':
    if(len(sys.argv) < 3):
        print "Usage : " + sys.argv[0] + " sat_solver file1.cnf file2.cnf ..."
        sys.exit(1)
    cmd = sys.argv[1]
    safe_solver = "minisat"
    for x in sys.argv[2:]:
        check(cmd, safe_solver, x)
    

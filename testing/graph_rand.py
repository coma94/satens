# -*- coding: utf-8 -*-
from bench import bench
import sys
from os import system

import numpy as np
import matplotlib.pyplot as plt

from multiprocessing import Process, Queue

nbtests = 5

from graph import process_bench


if __name__ == '__main__':
    if(len(sys.argv) < 3):
        print "Usage : " + sys.argv[0] + " solver1.. solverk -gen generator number"
        sys.exit(1)
    fig = plt.figure()

    solvers = []
    generator = ""
    number_files = int(sys.argv[-1])

    # Parse les arguments (ordre !)
    files_passed = False
    for elem in sys.argv[1:]:
        if(elem=="-gen" or elem=="--gen"):
            files_passed = True
        elif(not files_passed):
            solvers.append(elem)
        else:
            generator = elem
            break


    # Axes de la figure
    ax = fig.add_subplot(111)
    ax.set_ylabel("temps (s)")
    ax.set_xlabel("poffset")
    fig.subplots_adjust(top=0.85)

    color=['b', 'g', 'r', 'c', 'm', 'y', 'k']

    
    datas = {}
    errors = {}
    x = []

    for solver in solvers:
        datas[solver] = []
        errors[solver] = []

    #gen data
    for i in range(0,number_files):
        # On génère le fichier à coup de system()
        print (number_files-i)/((number_files)*10.)
        system(generator + " 20 " + str((number_files-i)/((number_files)*10.)) + " >> /tmp/temp_gen.cnf")
        
        try:
            q = Queue()
            procs = []

            #start process
            for solver in solvers:
                procs.append(Process( target=process_bench, args= (solver, "/tmp/temp_gen.cnf", q) ))
                procs[-1].start()

            #join process
            for p in procs:
                p.join()

            # get res
            while not q.empty():
                (solver, time, ecart) = q.get()
                datas[solver].append(time)
                errors[solver].append(ecart)

            # On ajoute la bone abcisse
            x.append((number_files-i)/((number_files)*10.))

            # on supprime le fichier crée
            system("rm /tmp/temp_gen.cnf")
            
        except KeyboardInterrupt:
            print("KeyboardInterrupt !")
            break

    # print data
    for i,solver in enumerate(solvers):
#        plt.plot(datas[solver], color[i % 7], label=solver)
        plt.errorbar(x, datas[solver], yerr=errors[solver], color=color[i%7], label=solver)
        
    plt.legend()
    plt.show()

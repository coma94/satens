#include <cmath>
#include <set>
#include <iostream>
#include <string>
#include <random>
#include <vector>

/**
 * \brief on crée un problème SAT-RB, comme précisé ici : http://www.univ-orleans.fr/evenements/jfpc/Articles/paper_07.pdf . Les paramètres que l'on modifie sont n (l'ordre de grandeur du nombre de variable, qui est en fait n**(alpha+1)), et p, qui permet de savoir si l'on est près où non de la limite sat/unsat
 */
void make_satmb(int n, double alpha, double r, double p)
{
	double nad = pow(n, alpha);
	int na = (int) nad;
	int nv = 1;
	std::vector<std::vector<int> > formules;
	formules.resize(n);
	// Etape 1
	for(int i = 0; i < n; i++)
	{
		for(int j = 0; j < na; j++)
		{
			formules[i].push_back(nv);
			nv++;
		}
		for(int j : formules[i])
		{
			for(int k : formules[i])
			{
				if(j < k)
					formules.push_back({-j, -k});
			}
		}
	}
	// Etape 2
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<> ens(0, n-1);
	std::uniform_int_distribution<> pos(0, na-1);
	std::set<std::vector<int> > contraintes;
	std::vector<int> tmp;
	double tc = p*pow(n, 2*alpha);
	double nstep3 = r*n*log(n);
	int n3 = (int) nstep3;
	int t_contraintes = (int) tc;
	int e1 = 0;
	int e2 = 0;
	int swap;
	for(int i = 1; i < n3; i++)
	{
		contraintes.clear();
		e1 = ens(gen);
		e2 = ens(gen);
		while(e1 == e2)
			e2 = ens(gen);
		if(e1 > e2)
		{
			swap = e1;
			e1 = e2;
			e2 = swap;
		}
		while(contraintes.size() < t_contraintes)
		{
			tmp.clear();
			tmp.push_back(-formules[e1][pos(gen)]);
			tmp.push_back(-formules[e2][pos(gen)]);
			contraintes.insert(tmp);
		}
		for(auto it = contraintes.begin(); it != contraintes.end(); ++it)
		{
			formules.push_back(*it);
		}
	}
	std::cout << "p cnf " << nv-1 << " " << formules.size() << std::endl;
	for(int i = 0; i < formules.size(); i++)
	{
		for(int j = 0; j < formules[i].size(); j++)
		{
			std::cout << formules[i][j] << " ";
		}
		std::cout << "0\n";
	}
}


int main(int nargs, char** args)
{
	if(nargs != 3)
	{
		std::cout << "Usage : " << args[0] << " n poffset\n";
		return 1;
	}
	int n = atoi(args[1]);
	double alpha = 0.8;
	double r = 1.5; // a 3 : c'est carrément un massacre
	double p = 1-exp(-alpha/r)-atof(args[2]);//-0.0001;
	std::cout << "c p=" << p << std::endl;
	make_satmb(n, alpha, r, p);
}

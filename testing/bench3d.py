# -*- coding: utf-8 -*-
from bench import bench
import sys

import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib import cm

def getinfo(f):
    g = f.split("_")
    return int(g[2]), int(g[3])


if __name__ == '__main__':
    if(len(sys.argv) < 3):
        print "Usage : " + sys.argv[0] + " files.cnf"
        sys.exit(1)

    fig = plt.figure()
    ax = fig.gca(projection='3d')

    ax.set_xlabel('nb var')
    ax.set_ylabel('nb cl')
    ax.set_zlabel('t')
    cnffiles = sys.argv[1:]

    data_minisat = []
    data_resol = []
    data_x = []
    data_y = []
    
    for f in cnffiles:
        x, y = getinfo(f)
        data_x.append(x)
        data_y.append(y)
        print "minisat:  " + f
        t_minisat = bench(["minisat"]+[f])
        print "resol:    " + f
        t_resol = bench(["./resol", "--cl"]+[f])
        data_minisat.append(t_minisat)
        data_resol.append(t_resol)
        ax.scatter(x,y,t_minisat, color='b')
        ax.scatter(x,y,t_resol, color='r')

#    ax.plot(data_x, data_y, data_minisat, color='b', alpha = 0.1)#, label = "minisat") marchemaisinutile

    ax.plot_trisurf(data_x, data_y, data_minisat, color='b', alpha = 0.1)#, label = "minisat") marchemaismoche
    ax.plot_trisurf(data_x, data_y, data_resol, color='r', alpha = 0.3)#, lavel = "resol") marchemaismoche



#    ax.plot_surface(data_x, data_y, data_minisat, color='k', label="minisat") marche pas :(
#    ax.plot_surface(data_x, data_y, data_resol, color='r')#, label="resol") marche pas :(
#    plt.plot(data_minisat, 'k', label="minisat")
#    plt.plot(data_resol, 'r', label="resol")
#    plt.plot(data_resol_wl, 'm', label="resol-wl")
        
    plt.legend()
    plt.show()

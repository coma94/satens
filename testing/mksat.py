# -*- coding: utf-8 -*-
import subprocess
import sys

def mk_random_sat(n_var, n_clauses, bench_dir, niter=1000):
    if(n_clauses == 0):
        n_clauses = 5 * n_var
    sat_gen = "testing/gen_rand"
    fname = bench_dir + "gen_r_" + str(n_var) + "_" + str(n_clauses) + "_"
    for n in xrange(niter):
        f = open((fname + str(n) + ".cnf"), 'w')
        p = subprocess.Popen([sat_gen, str(n_var), str(n_clauses)], stdout = f)
        f.close()


if __name__ == '__main__':
    if(len(sys.argv) < 4):
        print "Usage : " + sys.argv[0] + " bench_dir n_var_min n_var_max (c_var_min) (c_var_max) (niter)" 
        sys.exit(1)
        
    bench_dir = sys.argv[1]
    n_var_min = int(sys.argv[2])
    n_var_max = int(sys.argv[3])
    nrep = 1
    if(len(sys.argv) >= 5):
        nrep = int(sys.argv[6])
        print sys.argv[6]
    if(len(sys.argv) >= 5):
        c_var_min = int(sys.argv[4])
        c_var_max = int(sys.argv[5])
        for n in xrange(n_var_min, n_var_max+1):
            for c in xrange(c_var_min, c_var_max+1):
                mk_random_sat(n, c, bench_dir, nrep)
    else:
        for n in xrange(n_var_min, n_var_max+1):
            mk_random_sat(n, 0, bench_dir, nrep)
